import { Component, OnInit } from '@angular/core';
import {TokenService} from '../services/token.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  loggedIn = false;

  constructor(private token: TokenService) { }

  ngOnInit() {
    if(this.token.getToken() !== '') {
      this.loggedIn = true;
    }
    this.token.loggedIn().subscribe(loggedIn => {
      this.loggedIn = loggedIn;
    });
  }

}
