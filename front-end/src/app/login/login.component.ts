import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    public loginForm: FormGroup;

    constructor(private auth: AuthService,
                private formBuilder: FormBuilder,
                private router: Router) { }

    ngOnInit() {
      this.loginForm = this.formBuilder.group({
          name: ['', Validators.required],
          password: ['', Validators.required]
      });
    }

    get f() { return this.loginForm.controls; }

    submitLoginForm() {
        console.log(this.f.name.value);
        console.log(this.f.password.value);
        if(this.loginForm.invalid) {
          return;
        }

        this.auth.login(this.f.name.value, this.f.password.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate(['/dashboard']);
                },
                error => {
                    console.log(error);
                }
            )
    }

}
