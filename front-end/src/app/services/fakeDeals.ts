
    [
        {
            "deal_id": 21246,
            "deal_amount": 9309.74,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20752,
            "deal_amount": 6050.34,
            "deal_quantity": 1,
            "deal_time": 1532930661,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20636,
            "deal_amount": 10551.73,
            "deal_quantity": 1,
            "deal_time": 1532930653,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20031,
            "deal_amount": 3581.63,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21864,
            "deal_amount": 4598.86,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21061,
            "deal_amount": 2986.71,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20697,
            "deal_amount": 3932.98,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21069,
            "deal_amount": 9312.4,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21950,
            "deal_amount": 9041.52,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21294,
            "deal_amount": 4910.04,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21776,
            "deal_amount": 3293.92,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21560,
            "deal_amount": 4594.23,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20515,
            "deal_amount": 3160.47,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20613,
            "deal_amount": 8545.67,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20448,
            "deal_amount": 1346.74,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20157,
            "deal_amount": 7443.82,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20961,
            "deal_amount": 5868.34,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20644,
            "deal_amount": 2468.36,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21890,
            "deal_amount": 2360.9,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21994,
            "deal_amount": 6090.83,
            "deal_quantity": 1,
            "deal_time": 1532933900,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20413,
            "deal_amount": 1382.66,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20979,
            "deal_amount": 1847.9,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21355,
            "deal_amount": 6248.88,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21089,
            "deal_amount": 9017.64,
            "deal_quantity": 1,
            "deal_time": 1532930819,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20549,
            "deal_amount": 3292,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21918,
            "deal_amount": 1112.75,
            "deal_quantity": 1,
            "deal_time": 1532931289,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21744,
            "deal_amount": 1347.85,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21347,
            "deal_amount": 9980.44,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20137,
            "deal_amount": 8987.47,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20438,
            "deal_amount": 2411.74,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20199,
            "deal_amount": 2582.45,
            "deal_quantity": 1,
            "deal_time": 1532930526,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21888,
            "deal_amount": 6266.78,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20433,
            "deal_amount": 3297.2,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21312,
            "deal_amount": 6183.77,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21960,
            "deal_amount": 6523.58,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20088,
            "deal_amount": 1696.87,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21151,
            "deal_amount": 498.51,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21179,
            "deal_amount": 4381.38,
            "deal_quantity": 1,
            "deal_time": 1532930834,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21822,
            "deal_amount": 9476.21,
            "deal_quantity": 1,
            "deal_time": 1532931269,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20180,
            "deal_amount": 9138.74,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21601,
            "deal_amount": 2410.51,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20908,
            "deal_amount": 1780.13,
            "deal_quantity": 1,
            "deal_time": 1532930749,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21103,
            "deal_amount": 480.28,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21044,
            "deal_amount": 1182.94,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21551,
            "deal_amount": 6636.45,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21530,
            "deal_amount": 4413.95,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21839,
            "deal_amount": 2725.34,
            "deal_quantity": 1,
            "deal_time": 1532931270,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21751,
            "deal_amount": 2504.07,
            "deal_quantity": 1,
            "deal_time": 1532930993,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20696,
            "deal_amount": 479.46,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20915,
            "deal_amount": 1784.26,
            "deal_quantity": 1,
            "deal_time": 1532930749,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20559,
            "deal_amount": 3113.54,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21859,
            "deal_amount": 3372.64,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21514,
            "deal_amount": 1463.3,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21442,
            "deal_amount": 1090.33,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20879,
            "deal_amount": 1137.86,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21802,
            "deal_amount": 2507.13,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21251,
            "deal_amount": 2733.33,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21598,
            "deal_amount": 2475.03,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20927,
            "deal_amount": 2973.27,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21184,
            "deal_amount": 6685.81,
            "deal_quantity": 1,
            "deal_time": 1532930834,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20774,
            "deal_amount": 1734.34,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20787,
            "deal_amount": 3191.08,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20100,
            "deal_amount": 8270.6,
            "deal_quantity": 1,
            "deal_time": 1532930513,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21099,
            "deal_amount": 486.57,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21429,
            "deal_amount": 4772.34,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20888,
            "deal_amount": 3088.62,
            "deal_quantity": 1,
            "deal_time": 1532930742,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20748,
            "deal_amount": 3178.97,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20478,
            "deal_amount": 3902.65,
            "deal_quantity": 1,
            "deal_time": 1532930648,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20309,
            "deal_amount": 7591.46,
            "deal_quantity": 1,
            "deal_time": 1532930582,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21503,
            "deal_amount": 1506.89,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20220,
            "deal_amount": 1639.21,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20885,
            "deal_amount": 3873.41,
            "deal_quantity": 1,
            "deal_time": 1532930742,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20370,
            "deal_amount": 7611.87,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20985,
            "deal_amount": 3069.8,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20260,
            "deal_amount": 5742.98,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20789,
            "deal_amount": 3007.29,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20310,
            "deal_amount": 8520.34,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20973,
            "deal_amount": 3071.61,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21952,
            "deal_amount": 3296.67,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20676,
            "deal_amount": 10098.82,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20480,
            "deal_amount": 428.54,
            "deal_quantity": 1,
            "deal_time": 1532930649,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20626,
            "deal_amount": 7917.62,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21269,
            "deal_amount": 9686.85,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21925,
            "deal_amount": 6530.38,
            "deal_quantity": 1,
            "deal_time": 1532931289,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21275,
            "deal_amount": 2839.19,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20814,
            "deal_amount": 8775.27,
            "deal_quantity": 1,
            "deal_time": 1532930678,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20468,
            "deal_amount": 8363.78,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21268,
            "deal_amount": 9601.81,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20505,
            "deal_amount": 3195.53,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21056,
            "deal_amount": 1729.48,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21766,
            "deal_amount": 1314.25,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20530,
            "deal_amount": 7961.35,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20059,
            "deal_amount": 3437.39,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21173,
            "deal_amount": 4407.6,
            "deal_quantity": 1,
            "deal_time": 1532930834,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21351,
            "deal_amount": 9938.42,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21689,
            "deal_amount": 8943.91,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20264,
            "deal_amount": 3440.66,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21951,
            "deal_amount": 2426.48,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20430,
            "deal_amount": 432.54,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20537,
            "deal_amount": 2381.96,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21411,
            "deal_amount": 1129.55,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20181,
            "deal_amount": 409.45,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20854,
            "deal_amount": 1159.52,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21636,
            "deal_amount": 513.94,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21825,
            "deal_amount": 2748.84,
            "deal_quantity": 1,
            "deal_time": 1532931269,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21534,
            "deal_amount": 4449.2,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21996,
            "deal_amount": 4613.63,
            "deal_quantity": 1,
            "deal_time": 1532933900,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20202,
            "deal_amount": 1662.98,
            "deal_quantity": 1,
            "deal_time": 1532930526,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21181,
            "deal_amount": 2459.35,
            "deal_quantity": 1,
            "deal_time": 1532930834,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21763,
            "deal_amount": 2502.13,
            "deal_quantity": 1,
            "deal_time": 1532930993,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21248,
            "deal_amount": 1178.92,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21604,
            "deal_amount": 4573.74,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20204,
            "deal_amount": 3290.69,
            "deal_quantity": 1,
            "deal_time": 1532930527,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21386,
            "deal_amount": 1609.4,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20287,
            "deal_amount": 1368.82,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21240,
            "deal_amount": 483.37,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21378,
            "deal_amount": 2652.24,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21924,
            "deal_amount": 8963.72,
            "deal_quantity": 1,
            "deal_time": 1532931289,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21931,
            "deal_amount": 501.15,
            "deal_quantity": 1,
            "deal_time": 1532931289,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20650,
            "deal_amount": 3072.9,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21547,
            "deal_amount": 2544.98,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20269,
            "deal_amount": 10382.49,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21717,
            "deal_amount": 3637.45,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20274,
            "deal_amount": 10248.37,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20588,
            "deal_amount": 5946.18,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20557,
            "deal_amount": 3147.46,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21448,
            "deal_amount": 6956.03,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21247,
            "deal_amount": 2453.35,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21285,
            "deal_amount": 476.69,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20464,
            "deal_amount": 430.81,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20278,
            "deal_amount": 7613.39,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21607,
            "deal_amount": 2794.94,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20395,
            "deal_amount": 1395.96,
            "deal_quantity": 1,
            "deal_time": 1532930588,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20725,
            "deal_amount": 10314.34,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21166,
            "deal_amount": 5988.28,
            "deal_quantity": 1,
            "deal_time": 1532930827,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21087,
            "deal_amount": 5809.85,
            "deal_quantity": 1,
            "deal_time": 1532930819,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20474,
            "deal_amount": 7910.36,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20811,
            "deal_amount": 6162.35,
            "deal_quantity": 1,
            "deal_time": 1532930678,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21753,
            "deal_amount": 507.36,
            "deal_quantity": 1,
            "deal_time": 1532930993,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21848,
            "deal_amount": 2347.63,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21645,
            "deal_amount": 2769.21,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20944,
            "deal_amount": 2594.19,
            "deal_quantity": 1,
            "deal_time": 1532930759,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21360,
            "deal_amount": 4884.44,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20692,
            "deal_amount": 6313.27,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20328,
            "deal_amount": 430.03,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20747,
            "deal_amount": 3043.11,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21713,
            "deal_amount": 2763.85,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21328,
            "deal_amount": 2641.27,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21111,
            "deal_amount": 4136.55,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21193,
            "deal_amount": 2462.68,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20606,
            "deal_amount": 8542.51,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21205,
            "deal_amount": 1681.57,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20002,
            "deal_amount": 8531.05,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20054,
            "deal_amount": 9959.99,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20112,
            "deal_amount": 3259.12,
            "deal_quantity": 1,
            "deal_time": 1532930514,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21491,
            "deal_amount": 9480.77,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20330,
            "deal_amount": 1684.49,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20619,
            "deal_amount": 6413.65,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20900,
            "deal_amount": 2599.15,
            "deal_quantity": 1,
            "deal_time": 1532930749,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20578,
            "deal_amount": 1631.99,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21643,
            "deal_amount": 9132.36,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21629,
            "deal_amount": 2803.32,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20090,
            "deal_amount": 3273.98,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20616,
            "deal_amount": 3111.11,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20968,
            "deal_amount": 2555.06,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21309,
            "deal_amount": 9940.52,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21932,
            "deal_amount": 3374.06,
            "deal_quantity": 1,
            "deal_time": 1532931289,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20407,
            "deal_amount": 8180.41,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21095,
            "deal_amount": 2540.92,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21803,
            "deal_amount": 8684.45,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20731,
            "deal_amount": 1694.72,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20949,
            "deal_amount": 5800.41,
            "deal_quantity": 1,
            "deal_time": 1532930760,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20850,
            "deal_amount": 8931.27,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20450,
            "deal_amount": 6023.64,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21333,
            "deal_amount": 463.31,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20258,
            "deal_amount": 8728.42,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21512,
            "deal_amount": 1480.91,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21432,
            "deal_amount": 480.88,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20097,
            "deal_amount": 3745.68,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21223,
            "deal_amount": 6807.35,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21273,
            "deal_amount": 9764.54,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21524,
            "deal_amount": 2554.58,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21402,
            "deal_amount": 1127.1,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20149,
            "deal_amount": 7974.65,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21045,
            "deal_amount": 1190.39,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21807,
            "deal_amount": 2723.69,
            "deal_quantity": 1,
            "deal_time": 1532931269,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21226,
            "deal_amount": 1701.43,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20244,
            "deal_amount": 7597.31,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21900,
            "deal_amount": 4710.48,
            "deal_quantity": 1,
            "deal_time": 1532931273,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21899,
            "deal_amount": 1094.97,
            "deal_quantity": 1,
            "deal_time": 1532931273,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20716,
            "deal_amount": 1688.12,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20455,
            "deal_amount": 3758.6,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20166,
            "deal_amount": 1408.93,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20348,
            "deal_amount": 5875.41,
            "deal_quantity": 1,
            "deal_time": 1532930584,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20068,
            "deal_amount": 3336.41,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21242,
            "deal_amount": 1193.53,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20456,
            "deal_amount": 443.4,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21679,
            "deal_amount": 3844.51,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20275,
            "deal_amount": 8440.77,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21886,
            "deal_amount": 3333.61,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21476,
            "deal_amount": 2771.79,
            "deal_quantity": 1,
            "deal_time": 1532930975,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20599,
            "deal_amount": 3137.9,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20701,
            "deal_amount": 10151.4,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21498,
            "deal_amount": 6746.09,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21574,
            "deal_amount": 2491.73,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20988,
            "deal_amount": 4003.19,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21257,
            "deal_amount": 4265.3,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21725,
            "deal_amount": 2734.25,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20865,
            "deal_amount": 3059.4,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20341,
            "deal_amount": 427.75,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21945,
            "deal_amount": 6159.43,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21740,
            "deal_amount": 2728.48,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21380,
            "deal_amount": 4561.94,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21887,
            "deal_amount": 9242.78,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21034,
            "deal_amount": 2527,
            "deal_quantity": 1,
            "deal_time": 1532930790,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21494,
            "deal_amount": 6781.11,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21681,
            "deal_amount": 4593.14,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21733,
            "deal_amount": 2761.31,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21121,
            "deal_amount": 2791.22,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20804,
            "deal_amount": 6094.04,
            "deal_quantity": 1,
            "deal_time": 1532930663,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20531,
            "deal_amount": 8046.09,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21396,
            "deal_amount": 7054.87,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21496,
            "deal_amount": 2770.63,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21654,
            "deal_amount": 2530.34,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21466,
            "deal_amount": 9771.71,
            "deal_quantity": 1,
            "deal_time": 1532930975,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21033,
            "deal_amount": 1789.06,
            "deal_quantity": 1,
            "deal_time": 1532930790,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20037,
            "deal_amount": 3546.77,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20796,
            "deal_amount": 3121.22,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20381,
            "deal_amount": 1687.25,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20770,
            "deal_amount": 1718.33,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20076,
            "deal_amount": 5716.11,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21532,
            "deal_amount": 9217.17,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21080,
            "deal_amount": 2518.29,
            "deal_quantity": 1,
            "deal_time": 1532930819,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20013,
            "deal_amount": 9614.48,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21208,
            "deal_amount": 9876.59,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20838,
            "deal_amount": 3060.32,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21027,
            "deal_amount": 1815.42,
            "deal_quantity": 1,
            "deal_time": 1532930789,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21578,
            "deal_amount": 6613.58,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21675,
            "deal_amount": 1422.23,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20519,
            "deal_amount": 3192.56,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21561,
            "deal_amount": 6763.36,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21997,
            "deal_amount": 1235.49,
            "deal_quantity": 1,
            "deal_time": 1532933900,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21813,
            "deal_amount": 6340.73,
            "deal_quantity": 1,
            "deal_time": 1532931269,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21051,
            "deal_amount": 10422.65,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21874,
            "deal_amount": 3375.05,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20349,
            "deal_amount": 422.8,
            "deal_quantity": 1,
            "deal_time": 1532930584,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21148,
            "deal_amount": 2958.92,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21020,
            "deal_amount": 497.65,
            "deal_quantity": 1,
            "deal_time": 1532930789,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20444,
            "deal_amount": 434.39,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20489,
            "deal_amount": 10218.48,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21708,
            "deal_amount": 9587.39,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21787,
            "deal_amount": 6165.79,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21648,
            "deal_amount": 6365.6,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20242,
            "deal_amount": 7897.2,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20359,
            "deal_amount": 3424.54,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21698,
            "deal_amount": 4610.3,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20719,
            "deal_amount": 7708.61,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21215,
            "deal_amount": 1220.94,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20410,
            "deal_amount": 1374.62,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20358,
            "deal_amount": 8217.87,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20649,
            "deal_amount": 1278.53,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20733,
            "deal_amount": 1219.55,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20957,
            "deal_amount": 3106.21,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21250,
            "deal_amount": 9743.88,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21597,
            "deal_amount": 512.97,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20040,
            "deal_amount": 5714.26,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20659,
            "deal_amount": 10314.41,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21616,
            "deal_amount": 3973.5,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20114,
            "deal_amount": 1751.61,
            "deal_quantity": 1,
            "deal_time": 1532930514,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21879,
            "deal_amount": 6266.37,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21727,
            "deal_amount": 1069.59,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20007,
            "deal_amount": 7908.21,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20997,
            "deal_amount": 10181.29,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21062,
            "deal_amount": 10313.39,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21709,
            "deal_amount": 2539.86,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20761,
            "deal_amount": 6084.4,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21901,
            "deal_amount": 6161.27,
            "deal_quantity": 1,
            "deal_time": 1532931273,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20672,
            "deal_amount": 8900.85,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21082,
            "deal_amount": 1691.21,
            "deal_quantity": 1,
            "deal_time": 1532930819,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21554,
            "deal_amount": 2477.03,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21433,
            "deal_amount": 9145.71,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20673,
            "deal_amount": 6504.53,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21875,
            "deal_amount": 8759.56,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21703,
            "deal_amount": 1414.68,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20793,
            "deal_amount": 1742.46,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21304,
            "deal_amount": 2502.4,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21071,
            "deal_amount": 1690.26,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21415,
            "deal_amount": 1115.07,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21191,
            "deal_amount": 2481.16,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21619,
            "deal_amount": 9208.08,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20322,
            "deal_amount": 420.04,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20640,
            "deal_amount": 1646.87,
            "deal_quantity": 1,
            "deal_time": 1532930654,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21652,
            "deal_amount": 1433.85,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20231,
            "deal_amount": 10302.46,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21922,
            "deal_amount": 2679.93,
            "deal_quantity": 1,
            "deal_time": 1532931289,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21230,
            "deal_amount": 491.67,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21097,
            "deal_amount": 9093.94,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21861,
            "deal_amount": 2395.97,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21857,
            "deal_amount": 1136.23,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21937,
            "deal_amount": 1276,
            "deal_quantity": 1,
            "deal_time": 1532931290,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21575,
            "deal_amount": 4246.78,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21587,
            "deal_amount": 1090.13,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21856,
            "deal_amount": 2476.13,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21846,
            "deal_amount": 4748.73,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21317,
            "deal_amount": 6245.07,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21635,
            "deal_amount": 9219.06,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20032,
            "deal_amount": 5768.93,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21956,
            "deal_amount": 6211.57,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20562,
            "deal_amount": 6878.03,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21975,
            "deal_amount": 9010.82,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20461,
            "deal_amount": 3811.8,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21431,
            "deal_amount": 2530.11,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20377,
            "deal_amount": 3483.35,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21459,
            "deal_amount": 9137.36,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21546,
            "deal_amount": 6703.58,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20912,
            "deal_amount": 6271.31,
            "deal_quantity": 1,
            "deal_time": 1532930749,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21413,
            "deal_amount": 4826.87,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21404,
            "deal_amount": 4915.04,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21794,
            "deal_amount": 6221.37,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20884,
            "deal_amount": 7392.47,
            "deal_quantity": 1,
            "deal_time": 1532930742,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20420,
            "deal_amount": 3459.67,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20124,
            "deal_amount": 401.24,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21820,
            "deal_amount": 3337.34,
            "deal_quantity": 1,
            "deal_time": 1532931269,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21375,
            "deal_amount": 1627.11,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20998,
            "deal_amount": 6905.89,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20364,
            "deal_amount": 2470.84,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21063,
            "deal_amount": 10207.27,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20907,
            "deal_amount": 3921.14,
            "deal_quantity": 1,
            "deal_time": 1532930749,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20866,
            "deal_amount": 2626.77,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21401,
            "deal_amount": 2610.92,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20843,
            "deal_amount": 3128.9,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21768,
            "deal_amount": 2482.74,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21057,
            "deal_amount": 5990.69,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20899,
            "deal_amount": 6266.92,
            "deal_quantity": 1,
            "deal_time": 1532930749,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20102,
            "deal_amount": 1399.21,
            "deal_quantity": 1,
            "deal_time": 1532930514,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20553,
            "deal_amount": 8621.98,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20227,
            "deal_amount": 10201.96,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21591,
            "deal_amount": 2415.71,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20019,
            "deal_amount": 10078.76,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21437,
            "deal_amount": 4417.64,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21177,
            "deal_amount": 494.57,
            "deal_quantity": 1,
            "deal_time": 1532930834,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21227,
            "deal_amount": 9749.9,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21552,
            "deal_amount": 1465.05,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21590,
            "deal_amount": 4181.32,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21777,
            "deal_amount": 3286.34,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21315,
            "deal_amount": 4380.71,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20717,
            "deal_amount": 3307.91,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20069,
            "deal_amount": 10034.79,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20837,
            "deal_amount": 510.13,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20490,
            "deal_amount": 1326.96,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20762,
            "deal_amount": 7703.76,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21911,
            "deal_amount": 6377.75,
            "deal_quantity": 1,
            "deal_time": 1532931273,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20130,
            "deal_amount": 2492.62,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20756,
            "deal_amount": 7782.47,
            "deal_quantity": 1,
            "deal_time": 1532930661,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21852,
            "deal_amount": 3340.02,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21141,
            "deal_amount": 5260.47,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21704,
            "deal_amount": 509.57,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20921,
            "deal_amount": 493.85,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20639,
            "deal_amount": 2525.02,
            "deal_quantity": 1,
            "deal_time": 1532930654,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20140,
            "deal_amount": 2517.57,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20093,
            "deal_amount": 5663.24,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21455,
            "deal_amount": 2745.42,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20950,
            "deal_amount": 7122.06,
            "deal_quantity": 1,
            "deal_time": 1532930760,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20292,
            "deal_amount": 3508.05,
            "deal_quantity": 1,
            "deal_time": 1532930573,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20373,
            "deal_amount": 7507.1,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21830,
            "deal_amount": 1124.85,
            "deal_quantity": 1,
            "deal_time": 1532931270,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20386,
            "deal_amount": 1689.91,
            "deal_quantity": 1,
            "deal_time": 1532930586,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21479,
            "deal_amount": 6539.93,
            "deal_quantity": 1,
            "deal_time": 1532930975,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20203,
            "deal_amount": 412.27,
            "deal_quantity": 1,
            "deal_time": 1532930526,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20385,
            "deal_amount": 3875.5,
            "deal_quantity": 1,
            "deal_time": 1532930586,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20755,
            "deal_amount": 2603.53,
            "deal_quantity": 1,
            "deal_time": 1532930661,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21719,
            "deal_amount": 1359.75,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20623,
            "deal_amount": 5889.13,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21610,
            "deal_amount": 2493.23,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20365,
            "deal_amount": 1412.45,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20345,
            "deal_amount": 3397.13,
            "deal_quantity": 1,
            "deal_time": 1532930584,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20833,
            "deal_amount": 3847.85,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20801,
            "deal_amount": 10168.31,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20812,
            "deal_amount": 6235.34,
            "deal_quantity": 1,
            "deal_time": 1532930678,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21557,
            "deal_amount": 4536.16,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21782,
            "deal_amount": 518.54,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21842,
            "deal_amount": 6552.3,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21746,
            "deal_amount": 497.23,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20751,
            "deal_amount": 10139.96,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20513,
            "deal_amount": 1302.32,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20163,
            "deal_amount": 5722.41,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20934,
            "deal_amount": 1820.58,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21854,
            "deal_amount": 8760.38,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20520,
            "deal_amount": 3152.6,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20148,
            "deal_amount": 1400.93,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20296,
            "deal_amount": 3801.5,
            "deal_quantity": 1,
            "deal_time": 1532930573,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20168,
            "deal_amount": 3266.91,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21789,
            "deal_amount": 2534.54,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21036,
            "deal_amount": 10237.38,
            "deal_quantity": 1,
            "deal_time": 1532930790,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20795,
            "deal_amount": 6028.56,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20782,
            "deal_amount": 5913.77,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21029,
            "deal_amount": 6223.91,
            "deal_quantity": 1,
            "deal_time": 1532930789,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21040,
            "deal_amount": 2988.24,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21090,
            "deal_amount": 3017.78,
            "deal_quantity": 1,
            "deal_time": 1532930820,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20423,
            "deal_amount": 8046.91,
            "deal_quantity": 1,
            "deal_time": 1532930596,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20085,
            "deal_amount": 9343.86,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21778,
            "deal_amount": 6163.62,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21558,
            "deal_amount": 1091.13,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21834,
            "deal_amount": 2421.74,
            "deal_quantity": 1,
            "deal_time": 1532931270,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21566,
            "deal_amount": 4233.12,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20671,
            "deal_amount": 2424.86,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21361,
            "deal_amount": 2554.65,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20023,
            "deal_amount": 8455.39,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20064,
            "deal_amount": 3400.13,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20693,
            "deal_amount": 3098.71,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21091,
            "deal_amount": 9105.41,
            "deal_quantity": 1,
            "deal_time": 1532930820,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21638,
            "deal_amount": 3978.72,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20991,
            "deal_amount": 10290.49,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21409,
            "deal_amount": 2671.65,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20268,
            "deal_amount": 3393.38,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21521,
            "deal_amount": 2586.03,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21018,
            "deal_amount": 1192.24,
            "deal_quantity": 1,
            "deal_time": 1532930789,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20435,
            "deal_amount": 2405.44,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21308,
            "deal_amount": 6940.9,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20592,
            "deal_amount": 10366.81,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21059,
            "deal_amount": 9221.91,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21940,
            "deal_amount": 1109.44,
            "deal_quantity": 1,
            "deal_time": 1532931290,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20845,
            "deal_amount": 3823.43,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20862,
            "deal_amount": 1167.5,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20776,
            "deal_amount": 487.2,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20817,
            "deal_amount": 8673.69,
            "deal_quantity": 1,
            "deal_time": 1532930680,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21311,
            "deal_amount": 1143.78,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20604,
            "deal_amount": 10461.28,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20466,
            "deal_amount": 427.68,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21075,
            "deal_amount": 491.21,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20344,
            "deal_amount": 3914.54,
            "deal_quantity": 1,
            "deal_time": 1532930584,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20516,
            "deal_amount": 7123.22,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20886,
            "deal_amount": 3097.31,
            "deal_quantity": 1,
            "deal_time": 1532930742,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20401,
            "deal_amount": 3497.59,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21721,
            "deal_amount": 2453.42,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20895,
            "deal_amount": 3130.17,
            "deal_quantity": 1,
            "deal_time": 1532930745,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20469,
            "deal_amount": 7997.04,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21736,
            "deal_amount": 9676.08,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21305,
            "deal_amount": 1661.26,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20970,
            "deal_amount": 486.43,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20841,
            "deal_amount": 3109.08,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20765,
            "deal_amount": 2559.16,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20679,
            "deal_amount": 3303.43,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20633,
            "deal_amount": 8067,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20257,
            "deal_amount": 8821.6,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20622,
            "deal_amount": 8700.76,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21126,
            "deal_amount": 2812,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21335,
            "deal_amount": 9391.51,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21936,
            "deal_amount": 8664.87,
            "deal_quantity": 1,
            "deal_time": 1532931290,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20339,
            "deal_amount": 7781.83,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20011,
            "deal_amount": 3590.63,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20270,
            "deal_amount": 3366.31,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21366,
            "deal_amount": 2743.82,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20821,
            "deal_amount": 10272.11,
            "deal_quantity": 1,
            "deal_time": 1532930687,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21690,
            "deal_amount": 3849.69,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21186,
            "deal_amount": 1194.48,
            "deal_quantity": 1,
            "deal_time": 1532930834,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20842,
            "deal_amount": 8851.27,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21840,
            "deal_amount": 3346.3,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21460,
            "deal_amount": 2773.25,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20437,
            "deal_amount": 8125.48,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20799,
            "deal_amount": 2527.93,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20192,
            "deal_amount": 3796.36,
            "deal_quantity": 1,
            "deal_time": 1532930526,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21765,
            "deal_amount": 6427.46,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20411,
            "deal_amount": 10144.66,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20058,
            "deal_amount": 1723.99,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20259,
            "deal_amount": 10487,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20823,
            "deal_amount": 3828.42,
            "deal_quantity": 1,
            "deal_time": 1532930687,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21009,
            "deal_amount": 3047.16,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20189,
            "deal_amount": 2556.24,
            "deal_quantity": 1,
            "deal_time": 1532930526,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21722,
            "deal_amount": 2475.12,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21731,
            "deal_amount": 1370.86,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20418,
            "deal_amount": 3330.47,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20256,
            "deal_amount": 3750.59,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20990,
            "deal_amount": 1164.57,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21452,
            "deal_amount": 2718.37,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20012,
            "deal_amount": 3563.22,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21620,
            "deal_amount": 2823.48,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20542,
            "deal_amount": 1632.67,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20462,
            "deal_amount": 440.14,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21938,
            "deal_amount": 2284.39,
            "deal_quantity": 1,
            "deal_time": 1532931290,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20543,
            "deal_amount": 5969.99,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21043,
            "deal_amount": 2828.09,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20658,
            "deal_amount": 8798.26,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20760,
            "deal_amount": 2579.45,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20119,
            "deal_amount": 3732.93,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21384,
            "deal_amount": 2751.65,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21942,
            "deal_amount": 2650.33,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21962,
            "deal_amount": 8837.37,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20514,
            "deal_amount": 3362.6,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20794,
            "deal_amount": 3037.37,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21621,
            "deal_amount": 6441.22,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20375,
            "deal_amount": 1678.3,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21903,
            "deal_amount": 1081.58,
            "deal_quantity": 1,
            "deal_time": 1532931273,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21400,
            "deal_amount": 469.68,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20732,
            "deal_amount": 3402.36,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21944,
            "deal_amount": 3336.76,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21663,
            "deal_amount": 2793.44,
            "deal_quantity": 1,
            "deal_time": 1532930985,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20556,
            "deal_amount": 9951.49,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20030,
            "deal_amount": 3531.12,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21662,
            "deal_amount": 2566.32,
            "deal_quantity": 1,
            "deal_time": 1532930985,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20969,
            "deal_amount": 485.75,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21092,
            "deal_amount": 9017.45,
            "deal_quantity": 1,
            "deal_time": 1532930820,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20067,
            "deal_amount": 3367.95,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20272,
            "deal_amount": 5692.92,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21732,
            "deal_amount": 3497.49,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20700,
            "deal_amount": 7901.88,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20629,
            "deal_amount": 4022.97,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20081,
            "deal_amount": 8391.03,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21917,
            "deal_amount": 6435.1,
            "deal_quantity": 1,
            "deal_time": 1532931289,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20158,
            "deal_amount": 2588.34,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21012,
            "deal_amount": 1168.59,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20745,
            "deal_amount": 482.35,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21144,
            "deal_amount": 4277.34,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20107,
            "deal_amount": 3563.72,
            "deal_quantity": 1,
            "deal_time": 1532930514,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21274,
            "deal_amount": 6110.76,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21933,
            "deal_amount": 1265.17,
            "deal_quantity": 1,
            "deal_time": 1532931289,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20027,
            "deal_amount": 3494.76,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21256,
            "deal_amount": 4209.94,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20334,
            "deal_amount": 1384.11,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21106,
            "deal_amount": 5757.06,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21978,
            "deal_amount": 4594.03,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21961,
            "deal_amount": 6148.63,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21438,
            "deal_amount": 2688.11,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20628,
            "deal_amount": 7990.86,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21593,
            "deal_amount": 1075.18,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21963,
            "deal_amount": 9116.63,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20360,
            "deal_amount": 3854.64,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20089,
            "deal_amount": 393.63,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20206,
            "deal_amount": 3320.14,
            "deal_quantity": 1,
            "deal_time": 1532930527,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20727,
            "deal_amount": 2506.1,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21094,
            "deal_amount": 1201.34,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20718,
            "deal_amount": 3338.91,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21188,
            "deal_amount": 9131.8,
            "deal_quantity": 1,
            "deal_time": 1532930834,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21231,
            "deal_amount": 489.24,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21318,
            "deal_amount": 10013.96,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21773,
            "deal_amount": 2547.52,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20053,
            "deal_amount": 413.35,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20035,
            "deal_amount": 405.6,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20881,
            "deal_amount": 1151.92,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20340,
            "deal_amount": 8398.06,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20147,
            "deal_amount": 3515.51,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20880,
            "deal_amount": 2571.92,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20187,
            "deal_amount": 10050.46,
            "deal_quantity": 1,
            "deal_time": 1532930526,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21453,
            "deal_amount": 2473.99,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21548,
            "deal_amount": 2771.54,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20075,
            "deal_amount": 1696.22,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21487,
            "deal_amount": 9597.19,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21398,
            "deal_amount": 2691.87,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20521,
            "deal_amount": 3299.67,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21490,
            "deal_amount": 2492.43,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21934,
            "deal_amount": 509.68,
            "deal_quantity": 1,
            "deal_time": 1532931289,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21506,
            "deal_amount": 492.18,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21054,
            "deal_amount": 4137.54,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21076,
            "deal_amount": 6767.21,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21545,
            "deal_amount": 511.98,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21694,
            "deal_amount": 4639.6,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20904,
            "deal_amount": 513.39,
            "deal_quantity": 1,
            "deal_time": 1532930749,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20151,
            "deal_amount": 3229.38,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21015,
            "deal_amount": 3019.02,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21774,
            "deal_amount": 8590.16,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21203,
            "deal_amount": 4283.36,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20463,
            "deal_amount": 436.57,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21233,
            "deal_amount": 9644.48,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21582,
            "deal_amount": 9115.38,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21291,
            "deal_amount": 5075.57,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21276,
            "deal_amount": 5042.74,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21454,
            "deal_amount": 2499.19,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21425,
            "deal_amount": 2555.47,
            "deal_quantity": 1,
            "deal_time": 1532930840,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20892,
            "deal_amount": 9102.82,
            "deal_quantity": 1,
            "deal_time": 1532930745,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20682,
            "deal_amount": 3327.62,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20312,
            "deal_amount": 5818.17,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21577,
            "deal_amount": 2800.3,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21659,
            "deal_amount": 9141.07,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20579,
            "deal_amount": 1648.84,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20432,
            "deal_amount": 436.12,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20870,
            "deal_amount": 505.87,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21123,
            "deal_amount": 4107.39,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20575,
            "deal_amount": 6773,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20982,
            "deal_amount": 6976.95,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20875,
            "deal_amount": 2600.78,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21865,
            "deal_amount": 1330.93,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21836,
            "deal_amount": 512.89,
            "deal_quantity": 1,
            "deal_time": 1532931270,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20041,
            "deal_amount": 2496.62,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20108,
            "deal_amount": 3294.88,
            "deal_quantity": 1,
            "deal_time": 1532930514,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20095,
            "deal_amount": 1408.04,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20337,
            "deal_amount": 2494.4,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20327,
            "deal_amount": 10287.36,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21468,
            "deal_amount": 6863.4,
            "deal_quantity": 1,
            "deal_time": 1532930975,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20280,
            "deal_amount": 1669.7,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20972,
            "deal_amount": 9200.7,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21301,
            "deal_amount": 1651.11,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21624,
            "deal_amount": 1085.85,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21711,
            "deal_amount": 2475.02,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21483,
            "deal_amount": 9708.05,
            "deal_quantity": 1,
            "deal_time": 1532930975,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21024,
            "deal_amount": 9306.3,
            "deal_quantity": 1,
            "deal_time": 1532930789,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21041,
            "deal_amount": 4030.88,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21764,
            "deal_amount": 2520.89,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20184,
            "deal_amount": 1662.36,
            "deal_quantity": 1,
            "deal_time": 1532930526,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21336,
            "deal_amount": 4432.63,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20470,
            "deal_amount": 3855.2,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20105,
            "deal_amount": 9208.32,
            "deal_quantity": 1,
            "deal_time": 1532930514,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20057,
            "deal_amount": 7681.23,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20602,
            "deal_amount": 447.09,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21221,
            "deal_amount": 5297.17,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20922,
            "deal_amount": 3004.95,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20236,
            "deal_amount": 5613.72,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20352,
            "deal_amount": 7727.53,
            "deal_quantity": 1,
            "deal_time": 1532930584,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21180,
            "deal_amount": 6107.12,
            "deal_quantity": 1,
            "deal_time": 1532930834,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21164,
            "deal_amount": 497.78,
            "deal_quantity": 1,
            "deal_time": 1532930827,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21555,
            "deal_amount": 2516.48,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21207,
            "deal_amount": 6661.34,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21372,
            "deal_amount": 1638.41,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21192,
            "deal_amount": 6118.38,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20298,
            "deal_amount": 2444.11,
            "deal_quantity": 1,
            "deal_time": 1532930573,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20441,
            "deal_amount": 10327.89,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20938,
            "deal_amount": 2952.03,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21625,
            "deal_amount": 1445.31,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21388,
            "deal_amount": 2777.52,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20317,
            "deal_amount": 3849.62,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20234,
            "deal_amount": 8938.99,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21843,
            "deal_amount": 3372.89,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20897,
            "deal_amount": 506.7,
            "deal_quantity": 1,
            "deal_time": 1532930745,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20050,
            "deal_amount": 5778.61,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20063,
            "deal_amount": 3568.14,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21651,
            "deal_amount": 6300.4,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20911,
            "deal_amount": 2542.9,
            "deal_quantity": 1,
            "deal_time": 1532930749,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20533,
            "deal_amount": 7052.24,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20484,
            "deal_amount": 7354.6,
            "deal_quantity": 1,
            "deal_time": 1532930649,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20214,
            "deal_amount": 8983.16,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21319,
            "deal_amount": 2538.27,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20896,
            "deal_amount": 3089.2,
            "deal_quantity": 1,
            "deal_time": 1532930745,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21622,
            "deal_amount": 6373.76,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20428,
            "deal_amount": 10224.05,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20458,
            "deal_amount": 7461.95,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20103,
            "deal_amount": 7597.88,
            "deal_quantity": 1,
            "deal_time": 1532930514,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21488,
            "deal_amount": 6612.16,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21666,
            "deal_amount": 3838.32,
            "deal_quantity": 1,
            "deal_time": 1532930985,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20890,
            "deal_amount": 7306.91,
            "deal_quantity": 1,
            "deal_time": 1532930742,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20561,
            "deal_amount": 450.97,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21137,
            "deal_amount": 2743.14,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21353,
            "deal_amount": 2706.11,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20324,
            "deal_amount": 3488.58,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21680,
            "deal_amount": 1427.99,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20408,
            "deal_amount": 8236.92,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21706,
            "deal_amount": 3774.96,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21484,
            "deal_amount": 6851.45,
            "deal_quantity": 1,
            "deal_time": 1532930975,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20384,
            "deal_amount": 3822.05,
            "deal_quantity": 1,
            "deal_time": 1532930586,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20039,
            "deal_amount": 403.95,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21022,
            "deal_amount": 2503.65,
            "deal_quantity": 1,
            "deal_time": 1532930789,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20690,
            "deal_amount": 3130.02,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20600,
            "deal_amount": 3310.76,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21833,
            "deal_amount": 9377.21,
            "deal_quantity": 1,
            "deal_time": 1532931270,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20422,
            "deal_amount": 7441.68,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20094,
            "deal_amount": 3326.13,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20593,
            "deal_amount": 6586.9,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21258,
            "deal_amount": 1728.69,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20152,
            "deal_amount": 3261.81,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21058,
            "deal_amount": 6838.7,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21735,
            "deal_amount": 1081.07,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20819,
            "deal_amount": 7374.38,
            "deal_quantity": 1,
            "deal_time": 1532930682,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20217,
            "deal_amount": 420.11,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21235,
            "deal_amount": 9222.01,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20903,
            "deal_amount": 5836.01,
            "deal_quantity": 1,
            "deal_time": 1532930749,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20380,
            "deal_amount": 3502.72,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20685,
            "deal_amount": 2495.26,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20389,
            "deal_amount": 3533.56,
            "deal_quantity": 1,
            "deal_time": 1532930588,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20229,
            "deal_amount": 5605.63,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20161,
            "deal_amount": 3299.33,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20191,
            "deal_amount": 5719.26,
            "deal_quantity": 1,
            "deal_time": 1532930526,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21588,
            "deal_amount": 4556.57,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21920,
            "deal_amount": 505.17,
            "deal_quantity": 1,
            "deal_time": 1532931289,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20404,
            "deal_amount": 10254.93,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20183,
            "deal_amount": 415.49,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20634,
            "deal_amount": 7961.77,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20285,
            "deal_amount": 428.16,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20792,
            "deal_amount": 2558.47,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20457,
            "deal_amount": 7419.56,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20703,
            "deal_amount": 2525.49,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21267,
            "deal_amount": 6807.36,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21743,
            "deal_amount": 505.23,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21905,
            "deal_amount": 3401.15,
            "deal_quantity": 1,
            "deal_time": 1532931273,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21132,
            "deal_amount": 5339.02,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20707,
            "deal_amount": 3067.56,
            "deal_quantity": 1,
            "deal_time": 1532930659,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21228,
            "deal_amount": 1208.5,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20674,
            "deal_amount": 2445.63,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20194,
            "deal_amount": 3498.19,
            "deal_quantity": 1,
            "deal_time": 1532930526,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20849,
            "deal_amount": 2571.81,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20986,
            "deal_amount": 2833.83,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21252,
            "deal_amount": 9637.24,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21138,
            "deal_amount": 2764.08,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21113,
            "deal_amount": 1183.78,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21131,
            "deal_amount": 5414.09,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21213,
            "deal_amount": 6729.27,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21784,
            "deal_amount": 9740.66,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21435,
            "deal_amount": 1554.32,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21741,
            "deal_amount": 2475.07,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20772,
            "deal_amount": 7599.06,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20743,
            "deal_amount": 1703.91,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20528,
            "deal_amount": 5970.38,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20273,
            "deal_amount": 3405.84,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21194,
            "deal_amount": 491.65,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20764,
            "deal_amount": 1209.78,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20491,
            "deal_amount": 1315.93,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21860,
            "deal_amount": 4664.23,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20569,
            "deal_amount": 1336.99,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20132,
            "deal_amount": 10011.99,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21072,
            "deal_amount": 5548.75,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20522,
            "deal_amount": 1596.78,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21000,
            "deal_amount": 6841.06,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21513,
            "deal_amount": 2624.4,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20665,
            "deal_amount": 1635.84,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21218,
            "deal_amount": 2871.73,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21999,
            "deal_amount": 3131.3,
            "deal_quantity": 1,
            "deal_time": 1532933900,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21979,
            "deal_amount": 514.51,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21443,
            "deal_amount": 4474.72,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21066,
            "deal_amount": 2547.95,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20405,
            "deal_amount": 5932.7,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21580,
            "deal_amount": 9215.12,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20117,
            "deal_amount": 7513.54,
            "deal_quantity": 1,
            "deal_time": 1532930514,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21664,
            "deal_amount": 9350.94,
            "deal_quantity": 1,
            "deal_time": 1532930985,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21327,
            "deal_amount": 4461.94,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21691,
            "deal_amount": 8838.8,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21195,
            "deal_amount": 6607.26,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20536,
            "deal_amount": 447.47,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21935,
            "deal_amount": 8581.28,
            "deal_quantity": 1,
            "deal_time": 1532931290,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20864,
            "deal_amount": 6325,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21630,
            "deal_amount": 6376.95,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20331,
            "deal_amount": 7676.82,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20894,
            "deal_amount": 3099.67,
            "deal_quantity": 1,
            "deal_time": 1532930745,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21169,
            "deal_amount": 2778.68,
            "deal_quantity": 1,
            "deal_time": 1532930827,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21486,
            "deal_amount": 1081.44,
            "deal_quantity": 1,
            "deal_time": 1532930975,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21349,
            "deal_amount": 10053.92,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21081,
            "deal_amount": 10287.41,
            "deal_quantity": 1,
            "deal_time": 1532930819,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21444,
            "deal_amount": 1074.28,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21381,
            "deal_amount": 2755.02,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21480,
            "deal_amount": 2446.93,
            "deal_quantity": 1,
            "deal_time": 1532930975,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21391,
            "deal_amount": 1129.49,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21535,
            "deal_amount": 2514.16,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21109,
            "deal_amount": 488.37,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21831,
            "deal_amount": 2375.96,
            "deal_quantity": 1,
            "deal_time": 1532931270,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21556,
            "deal_amount": 6573.45,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20374,
            "deal_amount": 428.9,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21065,
            "deal_amount": 2793.61,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20584,
            "deal_amount": 3933.32,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21862,
            "deal_amount": 4630.5,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21316,
            "deal_amount": 2511.05,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20261,
            "deal_amount": 8630.58,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21497,
            "deal_amount": 9126.3,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21383,
            "deal_amount": 4522.31,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20808,
            "deal_amount": 5806.46,
            "deal_quantity": 1,
            "deal_time": 1532930663,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20951,
            "deal_amount": 5818.14,
            "deal_quantity": 1,
            "deal_time": 1532930760,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20001,
            "deal_amount": 3405.29,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21926,
            "deal_amount": 4671.65,
            "deal_quantity": 1,
            "deal_time": 1532931289,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20262,
            "deal_amount": 1396.63,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20078,
            "deal_amount": 1394.16,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21581,
            "deal_amount": 6541.5,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20906,
            "deal_amount": 10320.8,
            "deal_quantity": 1,
            "deal_time": 1532930749,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21745,
            "deal_amount": 1092.12,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20614,
            "deal_amount": 5944.66,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20511,
            "deal_amount": 2359.31,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21461,
            "deal_amount": 6942.91,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20195,
            "deal_amount": 7959.84,
            "deal_quantity": 1,
            "deal_time": 1532930526,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21474,
            "deal_amount": 490.77,
            "deal_quantity": 1,
            "deal_time": 1532930975,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21915,
            "deal_amount": 2340.44,
            "deal_quantity": 1,
            "deal_time": 1532931289,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21872,
            "deal_amount": 2339.9,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21133,
            "deal_amount": 4211.21,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21010,
            "deal_amount": 6216.05,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21376,
            "deal_amount": 1597.06,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20193,
            "deal_amount": 416.09,
            "deal_quantity": 1,
            "deal_time": 1532930526,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21104,
            "deal_amount": 2984.45,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20916,
            "deal_amount": 505.36,
            "deal_quantity": 1,
            "deal_time": 1532930749,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20277,
            "deal_amount": 422.15,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20247,
            "deal_amount": 7676.95,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20582,
            "deal_amount": 6677.31,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21977,
            "deal_amount": 4634.18,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20288,
            "deal_amount": 1673.93,
            "deal_quantity": 1,
            "deal_time": 1532930573,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20150,
            "deal_amount": 9076.71,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21146,
            "deal_amount": 1681.21,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20022,
            "deal_amount": 398.71,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20122,
            "deal_amount": 9105.14,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21927,
            "deal_amount": 2656.37,
            "deal_quantity": 1,
            "deal_time": 1532931289,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20591,
            "deal_amount": 3105.82,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20387,
            "deal_amount": 426.34,
            "deal_quantity": 1,
            "deal_time": 1532930588,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20302,
            "deal_amount": 428.93,
            "deal_quantity": 1,
            "deal_time": 1532930573,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21004,
            "deal_amount": 5821.38,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21542,
            "deal_amount": 4371.16,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21869,
            "deal_amount": 8840.01,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20416,
            "deal_amount": 1361.16,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20065,
            "deal_amount": 1411.55,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20230,
            "deal_amount": 3444.07,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21611,
            "deal_amount": 1439.46,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20005,
            "deal_amount": 9838.68,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20861,
            "deal_amount": 10434.21,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20624,
            "deal_amount": 5945.37,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21130,
            "deal_amount": 10252.57,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20552,
            "deal_amount": 1627.86,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21976,
            "deal_amount": 2314.43,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21592,
            "deal_amount": 1458.64,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20809,
            "deal_amount": 1175.35,
            "deal_quantity": 1,
            "deal_time": 1532930663,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21202,
            "deal_amount": 2904.76,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20325,
            "deal_amount": 2468.67,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21537,
            "deal_amount": 6839.64,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20368,
            "deal_amount": 3830.98,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20382,
            "deal_amount": 3364.22,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20113,
            "deal_amount": 8313.27,
            "deal_quantity": 1,
            "deal_time": 1532930514,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21450,
            "deal_amount": 478.63,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21403,
            "deal_amount": 1598.27,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20216,
            "deal_amount": 3322.06,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20397,
            "deal_amount": 3329.52,
            "deal_quantity": 1,
            "deal_time": 1532930588,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21959,
            "deal_amount": 8746.4,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21441,
            "deal_amount": 6328.2,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21953,
            "deal_amount": 3266.08,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20271,
            "deal_amount": 2440.96,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21189,
            "deal_amount": 493.34,
            "deal_quantity": 1,
            "deal_time": 1532930834,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20104,
            "deal_amount": 3783.89,
            "deal_quantity": 1,
            "deal_time": 1532930514,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21775,
            "deal_amount": 2571.42,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21964,
            "deal_amount": 8930.78,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21152,
            "deal_amount": 2540.26,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21883,
            "deal_amount": 6332.86,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21281,
            "deal_amount": 1167.86,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20917,
            "deal_amount": 1170.15,
            "deal_quantity": 1,
            "deal_time": 1532930749,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20585,
            "deal_amount": 10092.45,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20165,
            "deal_amount": 3775.54,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20960,
            "deal_amount": 7041.79,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21422,
            "deal_amount": 7032.41,
            "deal_quantity": 1,
            "deal_time": 1532930840,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21608,
            "deal_amount": 4110.52,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20047,
            "deal_amount": 408.82,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20453,
            "deal_amount": 3786.94,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20447,
            "deal_amount": 8199.86,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21702,
            "deal_amount": 4661.75,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20987,
            "deal_amount": 9205.88,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21374,
            "deal_amount": 9710.3,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21749,
            "deal_amount": 3426.3,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21122,
            "deal_amount": 10160.08,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20051,
            "deal_amount": 5723.03,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20412,
            "deal_amount": 8300.04,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20419,
            "deal_amount": 7968.66,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21658,
            "deal_amount": 9459.69,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20818,
            "deal_amount": 5832.18,
            "deal_quantity": 1,
            "deal_time": 1532930680,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21271,
            "deal_amount": 6878.23,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21671,
            "deal_amount": 2578.16,
            "deal_quantity": 1,
            "deal_time": 1532930985,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21947,
            "deal_amount": 2411.68,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20237,
            "deal_amount": 8828.65,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21605,
            "deal_amount": 9297.14,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20353,
            "deal_amount": 7776.57,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20155,
            "deal_amount": 8964.15,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20974,
            "deal_amount": 6912.49,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20036,
            "deal_amount": 1786.98,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20771,
            "deal_amount": 7681.26,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20451,
            "deal_amount": 5969.61,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21627,
            "deal_amount": 507.34,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21871,
            "deal_amount": 509.43,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21562,
            "deal_amount": 2498.05,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21326,
            "deal_amount": 4406.17,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20153,
            "deal_amount": 2563.61,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20061,
            "deal_amount": 2524.7,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21142,
            "deal_amount": 490.67,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21255,
            "deal_amount": 4237.16,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20376,
            "deal_amount": 2415.95,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21667,
            "deal_amount": 9144.11,
            "deal_quantity": 1,
            "deal_time": 1532930985,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20784,
            "deal_amount": 5948.18,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20174,
            "deal_amount": 10183.83,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21253,
            "deal_amount": 9212.94,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21434,
            "deal_amount": 488.88,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21002,
            "deal_amount": 487.79,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20999,
            "deal_amount": 486.41,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21201,
            "deal_amount": 2769.3,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20239,
            "deal_amount": 2479.08,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20125,
            "deal_amount": 3700.27,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21904,
            "deal_amount": 1091.44,
            "deal_quantity": 1,
            "deal_time": 1532931273,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20576,
            "deal_amount": 5887.13,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20297,
            "deal_amount": 422.07,
            "deal_quantity": 1,
            "deal_time": 1532930573,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21710,
            "deal_amount": 6156.51,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21495,
            "deal_amount": 6830.61,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20015,
            "deal_amount": 7830.19,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20208,
            "deal_amount": 5660.99,
            "deal_quantity": 1,
            "deal_time": 1532930527,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20074,
            "deal_amount": 9924.2,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20300,
            "deal_amount": 5812.87,
            "deal_quantity": 1,
            "deal_time": 1532930573,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20529,
            "deal_amount": 1311.83,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21972,
            "deal_amount": 1111.33,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20956,
            "deal_amount": 2533.23,
            "deal_quantity": 1,
            "deal_time": 1532930760,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20008,
            "deal_amount": 9720.69,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21596,
            "deal_amount": 2506.26,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21079,
            "deal_amount": 5569.34,
            "deal_quantity": 1,
            "deal_time": 1532930819,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20212,
            "deal_amount": 3807.48,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20263,
            "deal_amount": 2463.94,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20926,
            "deal_amount": 6343.83,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21407,
            "deal_amount": 9240.81,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21395,
            "deal_amount": 9278.11,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21467,
            "deal_amount": 487.07,
            "deal_quantity": 1,
            "deal_time": 1532930975,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20066,
            "deal_amount": 412.53,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21101,
            "deal_amount": 2565.73,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21797,
            "deal_amount": 3362.93,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20046,
            "deal_amount": 3566.31,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21288,
            "deal_amount": 1679.57,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21855,
            "deal_amount": 9356.88,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21970,
            "deal_amount": 518.41,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21210,
            "deal_amount": 2873.86,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20159,
            "deal_amount": 5771.8,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21647,
            "deal_amount": 6309.96,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20893,
            "deal_amount": 3064.05,
            "deal_quantity": 1,
            "deal_time": 1532930745,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20354,
            "deal_amount": 7666.75,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21050,
            "deal_amount": 3015.72,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21853,
            "deal_amount": 2749.48,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20825,
            "deal_amount": 10358.57,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20196,
            "deal_amount": 3261.51,
            "deal_quantity": 1,
            "deal_time": 1532930526,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21617,
            "deal_amount": 3973.39,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21019,
            "deal_amount": 5728.56,
            "deal_quantity": 1,
            "deal_time": 1532930789,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20304,
            "deal_amount": 432.68,
            "deal_quantity": 1,
            "deal_time": 1532930573,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21641,
            "deal_amount": 3902.67,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21426,
            "deal_amount": 4846.99,
            "deal_quantity": 1,
            "deal_time": 1532930840,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21504,
            "deal_amount": 6558.38,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20126,
            "deal_amount": 9012.27,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20332,
            "deal_amount": 3510.23,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21847,
            "deal_amount": 6485.79,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20930,
            "deal_amount": 1810,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20648,
            "deal_amount": 8035.47,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21365,
            "deal_amount": 2725.95,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21981,
            "deal_amount": 2466.12,
            "deal_quantity": 1,
            "deal_time": 1532933900,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21696,
            "deal_amount": 511.02,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21891,
            "deal_amount": 3368.33,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21389,
            "deal_amount": 466.15,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20291,
            "deal_amount": 1676.55,
            "deal_quantity": 1,
            "deal_time": 1532930573,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20932,
            "deal_amount": 5864.2,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21724,
            "deal_amount": 3570.17,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21021,
            "deal_amount": 10272.35,
            "deal_quantity": 1,
            "deal_time": 1532930789,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20783,
            "deal_amount": 5926.12,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21569,
            "deal_amount": 520.3,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21895,
            "deal_amount": 511.42,
            "deal_quantity": 1,
            "deal_time": 1532931273,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21385,
            "deal_amount": 4890.7,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21817,
            "deal_amount": 1102.15,
            "deal_quantity": 1,
            "deal_time": 1532931269,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20486,
            "deal_amount": 439.06,
            "deal_quantity": 1,
            "deal_time": 1532930649,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20426,
            "deal_amount": 1366.33,
            "deal_quantity": 1,
            "deal_time": 1532930596,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20390,
            "deal_amount": 1713.49,
            "deal_quantity": 1,
            "deal_time": 1532930588,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20925,
            "deal_amount": 3934.48,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20721,
            "deal_amount": 1252.95,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20210,
            "deal_amount": 3482.52,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20162,
            "deal_amount": 7906.72,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20603,
            "deal_amount": 5943.74,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21473,
            "deal_amount": 9651.95,
            "deal_quantity": 1,
            "deal_time": 1532930975,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21564,
            "deal_amount": 4303.68,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21985,
            "deal_amount": 2485.94,
            "deal_quantity": 1,
            "deal_time": 1532933900,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20445,
            "deal_amount": 8074.6,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20867,
            "deal_amount": 1752.48,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20392,
            "deal_amount": 2388.37,
            "deal_quantity": 1,
            "deal_time": 1532930588,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20713,
            "deal_amount": 6243.7,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21150,
            "deal_amount": 2989.36,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20131,
            "deal_amount": 3780.64,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20492,
            "deal_amount": 7249.87,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20070,
            "deal_amount": 3610.39,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20853,
            "deal_amount": 3150.74,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20706,
            "deal_amount": 10250.12,
            "deal_quantity": 1,
            "deal_time": 1532930659,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21928,
            "deal_amount": 2682.23,
            "deal_quantity": 1,
            "deal_time": 1532931289,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20954,
            "deal_amount": 2566.81,
            "deal_quantity": 1,
            "deal_time": 1532930760,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20554,
            "deal_amount": 6981.36,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21828,
            "deal_amount": 1360.01,
            "deal_quantity": 1,
            "deal_time": 1532931270,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21714,
            "deal_amount": 2741.45,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21359,
            "deal_amount": 9197.94,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21525,
            "deal_amount": 4448.75,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20167,
            "deal_amount": 7357.83,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21634,
            "deal_amount": 1093.55,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20010,
            "deal_amount": 3438.5,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20250,
            "deal_amount": 7671.33,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21868,
            "deal_amount": 1130.17,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20218,
            "deal_amount": 8882.25,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21851,
            "deal_amount": 6530.26,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21867,
            "deal_amount": 1142.94,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20417,
            "deal_amount": 5981.95,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21755,
            "deal_amount": 9754.78,
            "deal_quantity": 1,
            "deal_time": 1532930993,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20728,
            "deal_amount": 2536.83,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20855,
            "deal_amount": 5760.91,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20618,
            "deal_amount": 6513.32,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21470,
            "deal_amount": 2598.76,
            "deal_quantity": 1,
            "deal_time": 1532930975,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20615,
            "deal_amount": 8628.05,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21112,
            "deal_amount": 483.69,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21030,
            "deal_amount": 6167.65,
            "deal_quantity": 1,
            "deal_time": 1532930790,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21529,
            "deal_amount": 2573.7,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21134,
            "deal_amount": 2562.31,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21728,
            "deal_amount": 2762.62,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20223,
            "deal_amount": 5660.24,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21176,
            "deal_amount": 2938.28,
            "deal_quantity": 1,
            "deal_time": 1532930834,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20729,
            "deal_amount": 10178.13,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20306,
            "deal_amount": 5873.58,
            "deal_quantity": 1,
            "deal_time": 1532930573,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20532,
            "deal_amount": 7017.53,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20558,
            "deal_amount": 1636.94,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21729,
            "deal_amount": 3502.57,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21067,
            "deal_amount": 1713.76,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20301,
            "deal_amount": 10300.87,
            "deal_quantity": 1,
            "deal_time": 1532930573,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20170,
            "deal_amount": 1425.46,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21818,
            "deal_amount": 2357.78,
            "deal_quantity": 1,
            "deal_time": 1532931269,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21500,
            "deal_amount": 1101.92,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21278,
            "deal_amount": 9858.6,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21516,
            "deal_amount": 1109.7,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21795,
            "deal_amount": 8675.8,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20255,
            "deal_amount": 8735.25,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21471,
            "deal_amount": 1521.92,
            "deal_quantity": 1,
            "deal_time": 1532930975,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20421,
            "deal_amount": 7550.29,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20785,
            "deal_amount": 5862.55,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21882,
            "deal_amount": 9344.7,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21786,
            "deal_amount": 1330.83,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21417,
            "deal_amount": 2617.06,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21464,
            "deal_amount": 9680.27,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21170,
            "deal_amount": 1688.75,
            "deal_quantity": 1,
            "deal_time": 1532930827,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21414,
            "deal_amount": 2583.78,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21523,
            "deal_amount": 2543.84,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21263,
            "deal_amount": 2502.51,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20781,
            "deal_amount": 9993.35,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21023,
            "deal_amount": 9215.13,
            "deal_quantity": 1,
            "deal_time": 1532930789,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21168,
            "deal_amount": 9130.86,
            "deal_quantity": 1,
            "deal_time": 1532930827,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20120,
            "deal_amount": 1733.65,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20847,
            "deal_amount": 2546.49,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20670,
            "deal_amount": 1246.69,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21705,
            "deal_amount": 4728.61,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21796,
            "deal_amount": 3341.43,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20281,
            "deal_amount": 7511.43,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20882,
            "deal_amount": 1166.37,
            "deal_quantity": 1,
            "deal_time": 1532930742,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21344,
            "deal_amount": 1132.56,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21037,
            "deal_amount": 1774.73,
            "deal_quantity": 1,
            "deal_time": 1532930790,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20226,
            "deal_amount": 7516.72,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20581,
            "deal_amount": 1671.56,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20859,
            "deal_amount": 3034.07,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21827,
            "deal_amount": 6628.99,
            "deal_quantity": 1,
            "deal_time": 1532931269,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21835,
            "deal_amount": 6546.35,
            "deal_quantity": 1,
            "deal_time": 1532931270,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20889,
            "deal_amount": 9011.2,
            "deal_quantity": 1,
            "deal_time": 1532930742,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21298,
            "deal_amount": 1693.16,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21965,
            "deal_amount": 3231.71,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20209,
            "deal_amount": 3291.67,
            "deal_quantity": 1,
            "deal_time": 1532930527,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20753,
            "deal_amount": 7865.08,
            "deal_quantity": 1,
            "deal_time": 1532930661,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21502,
            "deal_amount": 6806.14,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20143,
            "deal_amount": 1699.13,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20077,
            "deal_amount": 3306.62,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21330,
            "deal_amount": 2815.49,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21499,
            "deal_amount": 1092.87,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21866,
            "deal_amount": 3406.33,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20403,
            "deal_amount": 1683.67,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21343,
            "deal_amount": 474.25,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20769,
            "deal_amount": 3170.99,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20338,
            "deal_amount": 5819,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20695,
            "deal_amount": 5927.77,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20238,
            "deal_amount": 3475.78,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20923,
            "deal_amount": 2569.89,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20042,
            "deal_amount": 8339.95,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21568,
            "deal_amount": 2767.98,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21074,
            "deal_amount": 2821.68,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21003,
            "deal_amount": 1821.94,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20596,
            "deal_amount": 4026.77,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21048,
            "deal_amount": 4077.44,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20540,
            "deal_amount": 3083.85,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20891,
            "deal_amount": 3062.87,
            "deal_quantity": 1,
            "deal_time": 1532930742,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21988,
            "deal_amount": 1239.76,
            "deal_quantity": 1,
            "deal_time": 1532933900,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20687,
            "deal_amount": 3129.91,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20154,
            "deal_amount": 8021.98,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20517,
            "deal_amount": 3326.01,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20211,
            "deal_amount": 5730.79,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20129,
            "deal_amount": 1387.06,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20739,
            "deal_amount": 3351.49,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20848,
            "deal_amount": 1774.96,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21720,
            "deal_amount": 6608.81,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20677,
            "deal_amount": 1656.78,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20754,
            "deal_amount": 1715.85,
            "deal_quantity": 1,
            "deal_time": 1532930661,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21688,
            "deal_amount": 4651.71,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21156,
            "deal_amount": 5869.05,
            "deal_quantity": 1,
            "deal_time": 1532930827,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20653,
            "deal_amount": 10420.85,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21533,
            "deal_amount": 1470.87,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20992,
            "deal_amount": 495.75,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21287,
            "deal_amount": 2644.3,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21838,
            "deal_amount": 8858.99,
            "deal_quantity": 1,
            "deal_time": 1532931270,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20993,
            "deal_amount": 2796.79,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20898,
            "deal_amount": 1174.11,
            "deal_quantity": 1,
            "deal_time": 1532930745,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20595,
            "deal_amount": 3978.7,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20780,
            "deal_amount": 6022.04,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20538,
            "deal_amount": 442.4,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21158,
            "deal_amount": 4325.28,
            "deal_quantity": 1,
            "deal_time": 1532930827,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21769,
            "deal_amount": 6230.54,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20508,
            "deal_amount": 9889.74,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21954,
            "deal_amount": 1120.51,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20034,
            "deal_amount": 7756.48,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21685,
            "deal_amount": 6768.47,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20475,
            "deal_amount": 425.18,
            "deal_quantity": 1,
            "deal_time": 1532930648,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21014,
            "deal_amount": 2527.15,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20527,
            "deal_amount": 3117.1,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20311,
            "deal_amount": 8589.97,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20290,
            "deal_amount": 7649.23,
            "deal_quantity": 1,
            "deal_time": 1532930573,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20678,
            "deal_amount": 2469.42,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20887,
            "deal_amount": 5806.5,
            "deal_quantity": 1,
            "deal_time": 1532930742,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21364,
            "deal_amount": 469.93,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20284,
            "deal_amount": 2466.62,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21119,
            "deal_amount": 2827.16,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21350,
            "deal_amount": 2562.09,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20293,
            "deal_amount": 5748.12,
            "deal_quantity": 1,
            "deal_time": 1532930573,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20714,
            "deal_amount": 6032.03,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20790,
            "deal_amount": 5972.77,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20712,
            "deal_amount": 1244.72,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21295,
            "deal_amount": 4927.04,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20723,
            "deal_amount": 7763.67,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21656,
            "deal_amount": 9216.2,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21458,
            "deal_amount": 6390.44,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21006,
            "deal_amount": 1154.7,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20547,
            "deal_amount": 7860.44,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21339,
            "deal_amount": 2789.83,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21540,
            "deal_amount": 2799.29,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20851,
            "deal_amount": 6343.87,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21420,
            "deal_amount": 6379.56,
            "deal_quantity": 1,
            "deal_time": 1532930840,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20141,
            "deal_amount": 2487.46,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20024,
            "deal_amount": 1421.39,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21595,
            "deal_amount": 2385,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21912,
            "deal_amount": 1104.44,
            "deal_quantity": 1,
            "deal_time": 1532931273,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21570,
            "deal_amount": 1456.93,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21826,
            "deal_amount": 1339.34,
            "deal_quantity": 1,
            "deal_time": 1532931269,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21586,
            "deal_amount": 1078.44,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20608,
            "deal_amount": 8457.46,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21747,
            "deal_amount": 2516.47,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21600,
            "deal_amount": 2792.94,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21780,
            "deal_amount": 2437.79,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20329,
            "deal_amount": 3862.69,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21632,
            "deal_amount": 9301.93,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21565,
            "deal_amount": 2543.09,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20666,
            "deal_amount": 10199.06,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21423,
            "deal_amount": 6436.95,
            "deal_quantity": 1,
            "deal_time": 1532930840,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21501,
            "deal_amount": 2799.49,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21025,
            "deal_amount": 10347.72,
            "deal_quantity": 1,
            "deal_time": 1532930789,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20378,
            "deal_amount": 3804.89,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21153,
            "deal_amount": 502.66,
            "deal_quantity": 1,
            "deal_time": 1532930827,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21323,
            "deal_amount": 2618.44,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21612,
            "deal_amount": 4117.12,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20459,
            "deal_amount": 1339.26,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21140,
            "deal_amount": 1702.26,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20983,
            "deal_amount": 9109.47,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21550,
            "deal_amount": 516.7,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21863,
            "deal_amount": 2364.36,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21943,
            "deal_amount": 2673.41,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20598,
            "deal_amount": 1637.54,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20321,
            "deal_amount": 2443.13,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21946,
            "deal_amount": 2303.47,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20362,
            "deal_amount": 1403.45,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20655,
            "deal_amount": 2468.24,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21682,
            "deal_amount": 1079.27,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21919,
            "deal_amount": 3411.17,
            "deal_quantity": 1,
            "deal_time": 1532931289,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20139,
            "deal_amount": 9902.33,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21606,
            "deal_amount": 2767.11,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21167,
            "deal_amount": 2754.52,
            "deal_quantity": 1,
            "deal_time": 1532930827,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20084,
            "deal_amount": 404.18,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21738,
            "deal_amount": 6098.47,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20087,
            "deal_amount": 1679.8,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21244,
            "deal_amount": 4185.12,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20372,
            "deal_amount": 5933.4,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20539,
            "deal_amount": 5911.6,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21334,
            "deal_amount": 1623.43,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21982,
            "deal_amount": 8913.57,
            "deal_quantity": 1,
            "deal_time": 1532933900,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21628,
            "deal_amount": 6621.75,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20200,
            "deal_amount": 2553.9,
            "deal_quantity": 1,
            "deal_time": 1532930526,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21427,
            "deal_amount": 1100.86,
            "deal_quantity": 1,
            "deal_time": 1532930840,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20496,
            "deal_amount": 7296.71,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20299,
            "deal_amount": 2422.74,
            "deal_quantity": 1,
            "deal_time": 1532930573,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21792,
            "deal_amount": 2409.6,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20172,
            "deal_amount": 9055.77,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20724,
            "deal_amount": 6141.6,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20545,
            "deal_amount": 3266.13,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21379,
            "deal_amount": 6315.13,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21026,
            "deal_amount": 487.73,
            "deal_quantity": 1,
            "deal_time": 1532930789,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20495,
            "deal_amount": 10020.9,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20940,
            "deal_amount": 7147.03,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20839,
            "deal_amount": 2520.27,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21517,
            "deal_amount": 9534.49,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21110,
            "deal_amount": 2540.32,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21149,
            "deal_amount": 9113.67,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21791,
            "deal_amount": 6581.13,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21073,
            "deal_amount": 9211.31,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20966,
            "deal_amount": 5888.72,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21701,
            "deal_amount": 2569.54,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20586,
            "deal_amount": 10180.12,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20175,
            "deal_amount": 1703,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21697,
            "deal_amount": 1421.72,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21779,
            "deal_amount": 6229.4,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21955,
            "deal_amount": 2679.82,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20831,
            "deal_amount": 5737.98,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21788,
            "deal_amount": 9644.16,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20567,
            "deal_amount": 5821.3,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21139,
            "deal_amount": 5250.34,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20307,
            "deal_amount": 1376.73,
            "deal_quantity": 1,
            "deal_time": 1532930582,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21307,
            "deal_amount": 4327.35,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21332,
            "deal_amount": 2661.18,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20594,
            "deal_amount": 1646.42,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20409,
            "deal_amount": 3293.48,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20691,
            "deal_amount": 1237.15,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20080,
            "deal_amount": 10018.55,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20467,
            "deal_amount": 8276.73,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20989,
            "deal_amount": 9117.73,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20394,
            "deal_amount": 7890.07,
            "deal_quantity": 1,
            "deal_time": 1532930588,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20683,
            "deal_amount": 7930.99,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20283,
            "deal_amount": 1380.68,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20937,
            "deal_amount": 3103.07,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20958,
            "deal_amount": 10404.71,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20829,
            "deal_amount": 3095.74,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20091,
            "deal_amount": 3301.79,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21986,
            "deal_amount": 507.72,
            "deal_quantity": 1,
            "deal_time": 1532933900,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20425,
            "deal_amount": 6102.35,
            "deal_quantity": 1,
            "deal_time": 1532930596,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21190,
            "deal_amount": 1209.71,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20006,
            "deal_amount": 1425.97,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21910,
            "deal_amount": 3373.36,
            "deal_quantity": 1,
            "deal_time": 1532931273,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20791,
            "deal_amount": 10072.28,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21212,
            "deal_amount": 5274.81,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20910,
            "deal_amount": 7373.69,
            "deal_quantity": 1,
            "deal_time": 1532930749,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20550,
            "deal_amount": 7932.24,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20610,
            "deal_amount": 451.38,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21712,
            "deal_amount": 1387.55,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20506,
            "deal_amount": 7228.13,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21286,
            "deal_amount": 468.81,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20901,
            "deal_amount": 1791.52,
            "deal_quantity": 1,
            "deal_time": 1532930749,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21754,
            "deal_amount": 2541.37,
            "deal_quantity": 1,
            "deal_time": 1532930993,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20134,
            "deal_amount": 8913.04,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21723,
            "deal_amount": 2555.4,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21341,
            "deal_amount": 2690.46,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21457,
            "deal_amount": 1532.67,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21265,
            "deal_amount": 1710.22,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20941,
            "deal_amount": 7197.13,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20343,
            "deal_amount": 1400.11,
            "deal_quantity": 1,
            "deal_time": 1532930584,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20652,
            "deal_amount": 3296.27,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21649,
            "deal_amount": 1424.3,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20415,
            "deal_amount": 1669.1,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20215,
            "deal_amount": 7348.57,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20645,
            "deal_amount": 1269.21,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20026,
            "deal_amount": 9690.81,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21105,
            "deal_amount": 5483.54,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20874,
            "deal_amount": 7400.77,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21162,
            "deal_amount": 4353.26,
            "deal_quantity": 1,
            "deal_time": 1532930827,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20498,
            "deal_amount": 9913.9,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20118,
            "deal_amount": 8198.04,
            "deal_quantity": 1,
            "deal_time": 1532930514,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20048,
            "deal_amount": 3544.86,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20500,
            "deal_amount": 9993.74,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21990,
            "deal_amount": 3227.12,
            "deal_quantity": 1,
            "deal_time": 1532933900,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20092,
            "deal_amount": 3652.67,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21232,
            "deal_amount": 6164.09,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20572,
            "deal_amount": 3139.54,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20305,
            "deal_amount": 428.61,
            "deal_quantity": 1,
            "deal_time": 1532930573,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21352,
            "deal_amount": 7011.6,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21594,
            "deal_amount": 6603.51,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20643,
            "deal_amount": 2493.54,
            "deal_quantity": 1,
            "deal_time": 1532930654,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20315,
            "deal_amount": 3458.03,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21618,
            "deal_amount": 3975.85,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20574,
            "deal_amount": 10121.25,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20688,
            "deal_amount": 3166.64,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20133,
            "deal_amount": 7441.15,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21837,
            "deal_amount": 9455.84,
            "deal_quantity": 1,
            "deal_time": 1532931270,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20424,
            "deal_amount": 6035.13,
            "deal_quantity": 1,
            "deal_time": 1532930596,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21060,
            "deal_amount": 5923.46,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20171,
            "deal_amount": 3822.54,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21983,
            "deal_amount": 9025.46,
            "deal_quantity": 1,
            "deal_time": 1532933900,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20185,
            "deal_amount": 5660.79,
            "deal_quantity": 1,
            "deal_time": 1532930526,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20609,
            "deal_amount": 2493.92,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20852,
            "deal_amount": 2601.05,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21302,
            "deal_amount": 9296.37,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21599,
            "deal_amount": 9206.5,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21515,
            "deal_amount": 501.76,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20144,
            "deal_amount": 3758.31,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21756,
            "deal_amount": 2493.65,
            "deal_quantity": 1,
            "deal_time": 1532930993,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21987,
            "deal_amount": 6036.95,
            "deal_quantity": 1,
            "deal_time": 1532933900,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21377,
            "deal_amount": 4595.93,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21750,
            "deal_amount": 2482.89,
            "deal_quantity": 1,
            "deal_time": 1532930993,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20472,
            "deal_amount": 2353.1,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21272,
            "deal_amount": 483.35,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21613,
            "deal_amount": 4044.07,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20033,
            "deal_amount": 3523.58,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21507,
            "deal_amount": 502.88,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21007,
            "deal_amount": 6903.31,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21354,
            "deal_amount": 9294.84,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20241,
            "deal_amount": 1422.88,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21823,
            "deal_amount": 1110.4,
            "deal_quantity": 1,
            "deal_time": 1532931269,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21511,
            "deal_amount": 9211.48,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20221,
            "deal_amount": 2527.72,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20873,
            "deal_amount": 516.59,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20984,
            "deal_amount": 4034.67,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21772,
            "deal_amount": 2460.87,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20734,
            "deal_amount": 2560.38,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20347,
            "deal_amount": 1663.97,
            "deal_quantity": 1,
            "deal_time": 1532930584,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21804,
            "deal_amount": 8780.44,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20319,
            "deal_amount": 5761.9,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21801,
            "deal_amount": 8762.92,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20662,
            "deal_amount": 2470.03,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21264,
            "deal_amount": 4322.92,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20551,
            "deal_amount": 3116.61,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20314,
            "deal_amount": 2469.37,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21742,
            "deal_amount": 512.54,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20746,
            "deal_amount": 5972.99,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21958,
            "deal_amount": 1129.4,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20802,
            "deal_amount": 3096.05,
            "deal_quantity": 1,
            "deal_time": 1532930663,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21531,
            "deal_amount": 9527.53,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21991,
            "deal_amount": 4644.63,
            "deal_quantity": 1,
            "deal_time": 1532933900,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21338,
            "deal_amount": 4486.6,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20286,
            "deal_amount": 1664.13,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21028,
            "deal_amount": 5638.71,
            "deal_quantity": 1,
            "deal_time": 1532930789,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21716,
            "deal_amount": 6682.23,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20651,
            "deal_amount": 6445.99,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21283,
            "deal_amount": 6173.88,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20098,
            "deal_amount": 1726,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21989,
            "deal_amount": 3195.85,
            "deal_quantity": 1,
            "deal_time": 1532933900,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20589,
            "deal_amount": 5884.3,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20741,
            "deal_amount": 3269.61,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21039,
            "deal_amount": 10325.24,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20994,
            "deal_amount": 3038.96,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20333,
            "deal_amount": 1661.29,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21198,
            "deal_amount": 6056.38,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21889,
            "deal_amount": 1108.45,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20414,
            "deal_amount": 426.45,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21088,
            "deal_amount": 9119.88,
            "deal_quantity": 1,
            "deal_time": 1532930819,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20356,
            "deal_amount": 421.63,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21583,
            "deal_amount": 2440.9,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21858,
            "deal_amount": 2372.74,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21314,
            "deal_amount": 2483.33,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20918,
            "deal_amount": 499.52,
            "deal_quantity": 1,
            "deal_time": 1532930749,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21810,
            "deal_amount": 2473.19,
            "deal_quantity": 1,
            "deal_time": 1532931269,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21182,
            "deal_amount": 6051.04,
            "deal_quantity": 1,
            "deal_time": 1532930834,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20465,
            "deal_amount": 3291.79,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21262,
            "deal_amount": 2477.45,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21390,
            "deal_amount": 2579.38,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21085,
            "deal_amount": 5868.71,
            "deal_quantity": 1,
            "deal_time": 1532930819,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21673,
            "deal_amount": 9043.69,
            "deal_quantity": 1,
            "deal_time": 1532930985,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21808,
            "deal_amount": 514.46,
            "deal_quantity": 1,
            "deal_time": 1532931269,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21478,
            "deal_amount": 9042.21,
            "deal_quantity": 1,
            "deal_time": 1532930975,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20512,
            "deal_amount": 7832.03,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20830,
            "deal_amount": 1175.7,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21016,
            "deal_amount": 1178.56,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20577,
            "deal_amount": 3317.52,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21469,
            "deal_amount": 6925.52,
            "deal_quantity": 1,
            "deal_time": 1532930975,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21615,
            "deal_amount": 1423.56,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20265,
            "deal_amount": 3474.08,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21083,
            "deal_amount": 10390.25,
            "deal_quantity": 1,
            "deal_time": 1532930819,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21300,
            "deal_amount": 1671.15,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21052,
            "deal_amount": 1180.9,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20279,
            "deal_amount": 10330.35,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21655,
            "deal_amount": 3909.73,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20350,
            "deal_amount": 7817.95,
            "deal_quantity": 1,
            "deal_time": 1532930584,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21266,
            "deal_amount": 9725.76,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20704,
            "deal_amount": 475.41,
            "deal_quantity": 1,
            "deal_time": 1532930659,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21340,
            "deal_amount": 9780.62,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21995,
            "deal_amount": 2657.01,
            "deal_quantity": 1,
            "deal_time": 1532933900,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21850,
            "deal_amount": 4710.34,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21760,
            "deal_amount": 2512.46,
            "deal_quantity": 1,
            "deal_time": 1532930993,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21687,
            "deal_amount": 2427.78,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21449,
            "deal_amount": 6886.15,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21145,
            "deal_amount": 5927.12,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20043,
            "deal_amount": 9591.31,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21718,
            "deal_amount": 2710.11,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21876,
            "deal_amount": 504.01,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20116,
            "deal_amount": 1384.24,
            "deal_quantity": 1,
            "deal_time": 1532930514,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21757,
            "deal_amount": 8676.88,
            "deal_quantity": 1,
            "deal_time": 1532930993,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21129,
            "deal_amount": 485.97,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21161,
            "deal_amount": 2512.53,
            "deal_quantity": 1,
            "deal_time": 1532930827,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20336,
            "deal_amount": 7734.72,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20436,
            "deal_amount": 2384.31,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20767,
            "deal_amount": 10103.78,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20367,
            "deal_amount": 2445.56,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21844,
            "deal_amount": 2439.81,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21245,
            "deal_amount": 9402.05,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20055,
            "deal_amount": 9485.53,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20654,
            "deal_amount": 3990.36,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20564,
            "deal_amount": 8000.02,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20638,
            "deal_amount": 464.35,
            "deal_quantity": 1,
            "deal_time": 1532930654,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21974,
            "deal_amount": 1252.69,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21070,
            "deal_amount": 4088.21,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20017,
            "deal_amount": 8401.31,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21485,
            "deal_amount": 1529.37,
            "deal_quantity": 1,
            "deal_time": 1532930975,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21878,
            "deal_amount": 1316.85,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20106,
            "deal_amount": 7665.1,
            "deal_quantity": 1,
            "deal_time": 1532930514,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20964,
            "deal_amount": 2911.83,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20056,
            "deal_amount": 5663.82,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20446,
            "deal_amount": 7379.95,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21171,
            "deal_amount": 2964.86,
            "deal_quantity": 1,
            "deal_time": 1532930828,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20657,
            "deal_amount": 1655.84,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20699,
            "deal_amount": 3347.02,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20612,
            "deal_amount": 5882.69,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21699,
            "deal_amount": 6690.7,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21686,
            "deal_amount": 6218.63,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20931,
            "deal_amount": 7217.48,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20109,
            "deal_amount": 403.99,
            "deal_quantity": 1,
            "deal_time": 1532930514,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20981,
            "deal_amount": 2876.24,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21907,
            "deal_amount": 9054.74,
            "deal_quantity": 1,
            "deal_time": 1532931273,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20822,
            "deal_amount": 3863.3,
            "deal_quantity": 1,
            "deal_time": 1532930687,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20773,
            "deal_amount": 8924.77,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20962,
            "deal_amount": 6229.15,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20487,
            "deal_amount": 8450.99,
            "deal_quantity": 1,
            "deal_time": 1532930649,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20710,
            "deal_amount": 5975.84,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20355,
            "deal_amount": 3886.12,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21816,
            "deal_amount": 6643.6,
            "deal_quantity": 1,
            "deal_time": 1532931269,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20627,
            "deal_amount": 2529.93,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20049,
            "deal_amount": 1776.9,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21845,
            "deal_amount": 1349.82,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20800,
            "deal_amount": 3064.64,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20810,
            "deal_amount": 494,
            "deal_quantity": 1,
            "deal_time": 1532930678,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21631,
            "deal_amount": 4631.69,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20442,
            "deal_amount": 3324.51,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21292,
            "deal_amount": 4989.98,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20668,
            "deal_amount": 475.37,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21493,
            "deal_amount": 2518.07,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20111,
            "deal_amount": 5718.85,
            "deal_quantity": 1,
            "deal_time": 1532930514,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20393,
            "deal_amount": 3849.79,
            "deal_quantity": 1,
            "deal_time": 1532930588,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20605,
            "deal_amount": 1652.63,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20164,
            "deal_amount": 3800.18,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21909,
            "deal_amount": 6315.84,
            "deal_quantity": 1,
            "deal_time": 1532931273,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21154,
            "deal_amount": 10236.82,
            "deal_quantity": 1,
            "deal_time": 1532930827,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20510,
            "deal_amount": 5920.21,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21538,
            "deal_amount": 2596.36,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21214,
            "deal_amount": 495.16,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21520,
            "deal_amount": 9123.18,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21382,
            "deal_amount": 2726.47,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21639,
            "deal_amount": 2513.39,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21356,
            "deal_amount": 2582.67,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20460,
            "deal_amount": 6031.61,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20702,
            "deal_amount": 478.44,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21603,
            "deal_amount": 2434.09,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21241,
            "deal_amount": 9305.08,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21163,
            "deal_amount": 5931.31,
            "deal_quantity": 1,
            "deal_time": 1532930827,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20868,
            "deal_amount": 5787.07,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21870,
            "deal_amount": 6568.31,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20452,
            "deal_amount": 442.38,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20479,
            "deal_amount": 5972.62,
            "deal_quantity": 1,
            "deal_time": 1532930649,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20720,
            "deal_amount": 8988.37,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21812,
            "deal_amount": 4686.52,
            "deal_quantity": 1,
            "deal_time": 1532931269,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21730,
            "deal_amount": 2791.75,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21046,
            "deal_amount": 6045.85,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20611,
            "deal_amount": 6487.5,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21220,
            "deal_amount": 9221.1,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20978,
            "deal_amount": 4056.8,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21329,
            "deal_amount": 9908.68,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20233,
            "deal_amount": 5555.37,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20400,
            "deal_amount": 3817.55,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21346,
            "deal_amount": 2782.02,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20190,
            "deal_amount": 3297.82,
            "deal_quantity": 1,
            "deal_time": 1532930526,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20083,
            "deal_amount": 9453.93,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21017,
            "deal_amount": 490.83,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20245,
            "deal_amount": 7785.28,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20503,
            "deal_amount": 1615.66,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21393,
            "deal_amount": 2751.13,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21508,
            "deal_amount": 4602.88,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21948,
            "deal_amount": 2332.99,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20028,
            "deal_amount": 2465.05,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21783,
            "deal_amount": 2698.35,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21559,
            "deal_amount": 1448.35,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20566,
            "deal_amount": 2436.62,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21897,
            "deal_amount": 4646.42,
            "deal_quantity": 1,
            "deal_time": 1532931273,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20494,
            "deal_amount": 443.89,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20225,
            "deal_amount": 7430.78,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20383,
            "deal_amount": 429.57,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20858,
            "deal_amount": 10537.86,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20976,
            "deal_amount": 1840.16,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20541,
            "deal_amount": 453.59,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21447,
            "deal_amount": 486.07,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20587,
            "deal_amount": 10270.51,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21759,
            "deal_amount": 6169.82,
            "deal_quantity": 1,
            "deal_time": 1532930993,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20637,
            "deal_amount": 1300,
            "deal_quantity": 1,
            "deal_time": 1532930653,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21408,
            "deal_amount": 6973.66,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21107,
            "deal_amount": 2804,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20029,
            "deal_amount": 1818.39,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20251,
            "deal_amount": 2429.03,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21299,
            "deal_amount": 2524.08,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21225,
            "deal_amount": 9854.2,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20128,
            "deal_amount": 3738.51,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21064,
            "deal_amount": 5983.33,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21896,
            "deal_amount": 1305.55,
            "deal_quantity": 1,
            "deal_time": 1532931273,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21536,
            "deal_amount": 9123.33,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21930,
            "deal_amount": 1118.49,
            "deal_quantity": 1,
            "deal_time": 1532931289,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20363,
            "deal_amount": 7559.62,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20526,
            "deal_amount": 7900.94,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20711,
            "deal_amount": 7802.54,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21219,
            "deal_amount": 6771.72,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21832,
            "deal_amount": 3370.72,
            "deal_quantity": 1,
            "deal_time": 1532931270,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21387,
            "deal_amount": 6997.48,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20597,
            "deal_amount": 1321.54,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20936,
            "deal_amount": 4038.5,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21762,
            "deal_amount": 2698.11,
            "deal_quantity": 1,
            "deal_time": 1532930993,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21136,
            "deal_amount": 4225.54,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20952,
            "deal_amount": 6168.13,
            "deal_quantity": 1,
            "deal_time": 1532930760,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20169,
            "deal_amount": 3472.4,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20722,
            "deal_amount": 3067.82,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20485,
            "deal_amount": 3913.59,
            "deal_quantity": 1,
            "deal_time": 1532930649,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21799,
            "deal_amount": 519.57,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20698,
            "deal_amount": 8005.99,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21289,
            "deal_amount": 6113.95,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20872,
            "deal_amount": 518.69,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21306,
            "deal_amount": 1638.6,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21614,
            "deal_amount": 1074.34,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21279,
            "deal_amount": 476.4,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20427,
            "deal_amount": 8203.31,
            "deal_quantity": 1,
            "deal_time": 1532930596,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21824,
            "deal_amount": 6706.65,
            "deal_quantity": 1,
            "deal_time": 1532931269,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21522,
            "deal_amount": 6634.13,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20073,
            "deal_amount": 3533.9,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20249,
            "deal_amount": 2452.19,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21637,
            "deal_amount": 2408.11,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21031,
            "deal_amount": 486.47,
            "deal_quantity": 1,
            "deal_time": 1532930790,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20632,
            "deal_amount": 3103.35,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20630,
            "deal_amount": 2498.44,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20924,
            "deal_amount": 7284.23,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21222,
            "deal_amount": 2479.65,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21211,
            "deal_amount": 9964.33,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20440,
            "deal_amount": 7482.4,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20580,
            "deal_amount": 2439.48,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21115,
            "deal_amount": 5931.89,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21296,
            "deal_amount": 7000.58,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20072,
            "deal_amount": 1685.2,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20820,
            "deal_amount": 503.64,
            "deal_quantity": 1,
            "deal_time": 1532930687,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21127,
            "deal_amount": 2780.15,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21185,
            "deal_amount": 4350.91,
            "deal_quantity": 1,
            "deal_time": 1532930834,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20835,
            "deal_amount": 10454.21,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20737,
            "deal_amount": 2580.06,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20965,
            "deal_amount": 1166.06,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21418,
            "deal_amount": 4450.75,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21748,
            "deal_amount": 1329.36,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21342,
            "deal_amount": 2757.06,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20834,
            "deal_amount": 8762.6,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20570,
            "deal_amount": 1613.23,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20176,
            "deal_amount": 1684.17,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21695,
            "deal_amount": 2445.33,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21644,
            "deal_amount": 9490.38,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20213,
            "deal_amount": 9086.55,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21877,
            "deal_amount": 9255.25,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20295,
            "deal_amount": 10409.74,
            "deal_quantity": 1,
            "deal_time": 1532930573,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21358,
            "deal_amount": 1625.02,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20694,
            "deal_amount": 1669.21,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21653,
            "deal_amount": 1084.66,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21576,
            "deal_amount": 1474.68,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20681,
            "deal_amount": 6411.04,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20975,
            "deal_amount": 3102.25,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21829,
            "deal_amount": 6331.42,
            "deal_quantity": 1,
            "deal_time": 1532931270,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21261,
            "deal_amount": 2450.7,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20571,
            "deal_amount": 2464.37,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21100,
            "deal_amount": 6643.97,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21199,
            "deal_amount": 6105.04,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20980,
            "deal_amount": 6282.72,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21216,
            "deal_amount": 2847.29,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21672,
            "deal_amount": 6711.58,
            "deal_quantity": 1,
            "deal_time": 1532930985,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20369,
            "deal_amount": 1667.05,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21505,
            "deal_amount": 9564.64,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21993,
            "deal_amount": 1257.71,
            "deal_quantity": 1,
            "deal_time": 1532933900,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21472,
            "deal_amount": 494.16,
            "deal_quantity": 1,
            "deal_time": 1532930975,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21609,
            "deal_amount": 9302.68,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20876,
            "deal_amount": 9104.02,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20507,
            "deal_amount": 2332.43,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21217,
            "deal_amount": 2451.55,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20738,
            "deal_amount": 1681.28,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20276,
            "deal_amount": 429.31,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20548,
            "deal_amount": 9873.11,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21416,
            "deal_amount": 4486.22,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20224,
            "deal_amount": 2505.32,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20759,
            "deal_amount": 10221.76,
            "deal_quantity": 1,
            "deal_time": 1532930661,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20955,
            "deal_amount": 488.58,
            "deal_quantity": 1,
            "deal_time": 1532930760,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20914,
            "deal_amount": 10222.77,
            "deal_quantity": 1,
            "deal_time": 1532930749,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21602,
            "deal_amount": 1060.29,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20082,
            "deal_amount": 10112.78,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21815,
            "deal_amount": 9479.11,
            "deal_quantity": 1,
            "deal_time": 1532931269,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20660,
            "deal_amount": 6475.82,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21209,
            "deal_amount": 2421.08,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20902,
            "deal_amount": 1186.14,
            "deal_quantity": 1,
            "deal_time": 1532930749,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20135,
            "deal_amount": 1746.09,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21726,
            "deal_amount": 4697.05,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21640,
            "deal_amount": 6653.86,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20583,
            "deal_amount": 10217.44,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20366,
            "deal_amount": 10252.26,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20826,
            "deal_amount": 2523.56,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21397,
            "deal_amount": 9345.71,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20953,
            "deal_amount": 3071.89,
            "deal_quantity": 1,
            "deal_time": 1532930760,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21196,
            "deal_amount": 2445.53,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20079,
            "deal_amount": 9578.75,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20590,
            "deal_amount": 3343.33,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20248,
            "deal_amount": 1385.24,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21345,
            "deal_amount": 9884.85,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20246,
            "deal_amount": 1401.33,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20995,
            "deal_amount": 6209.91,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21086,
            "deal_amount": 6708.61,
            "deal_quantity": 1,
            "deal_time": 1532930819,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20201,
            "deal_amount": 414.02,
            "deal_quantity": 1,
            "deal_time": 1532930526,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20565,
            "deal_amount": 2407.79,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21237,
            "deal_amount": 6880.41,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21096,
            "deal_amount": 2840.73,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21692,
            "deal_amount": 4713.48,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21668,
            "deal_amount": 3836.67,
            "deal_quantity": 1,
            "deal_time": 1532930985,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21626,
            "deal_amount": 6438.65,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21053,
            "deal_amount": 6913.46,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21906,
            "deal_amount": 8581.29,
            "deal_quantity": 1,
            "deal_time": 1532931273,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21544,
            "deal_amount": 1478.87,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20909,
            "deal_amount": 2572.15,
            "deal_quantity": 1,
            "deal_time": 1532930749,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20827,
            "deal_amount": 7437.71,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20219,
            "deal_amount": 10125.39,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21325,
            "deal_amount": 6306.73,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21980,
            "deal_amount": 1236.36,
            "deal_quantity": 1,
            "deal_time": 1532933900,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21224,
            "deal_amount": 9312.04,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20546,
            "deal_amount": 4030.33,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21623,
            "deal_amount": 6691.44,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20797,
            "deal_amount": 5895.15,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20406,
            "deal_amount": 427.23,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21373,
            "deal_amount": 1139.2,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20563,
            "deal_amount": 5896.18,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20806,
            "deal_amount": 1191.95,
            "deal_quantity": 1,
            "deal_time": 1532930663,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20778,
            "deal_amount": 7516.62,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21660,
            "deal_amount": 9236.14,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21068,
            "deal_amount": 10300.67,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20779,
            "deal_amount": 7441.93,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20646,
            "deal_amount": 465.5,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20232,
            "deal_amount": 8004.17,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21282,
            "deal_amount": 2666.04,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21821,
            "deal_amount": 6264.61,
            "deal_quantity": 1,
            "deal_time": 1532931269,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20736,
            "deal_amount": 7820.71,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21579,
            "deal_amount": 2468.16,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21633,
            "deal_amount": 6308.13,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21297,
            "deal_amount": 1153.01,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21737,
            "deal_amount": 6531.45,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20905,
            "deal_amount": 6337.16,
            "deal_quantity": 1,
            "deal_time": 1532930749,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21362,
            "deal_amount": 4532.64,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20267,
            "deal_amount": 8533.06,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21116,
            "deal_amount": 9007.6,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20253,
            "deal_amount": 7594.26,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21120,
            "deal_amount": 5868.1,
            "deal_quantity": 1,
            "deal_time": 1532930825,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21399,
            "deal_amount": 4902.07,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21929,
            "deal_amount": 6596.18,
            "deal_quantity": 1,
            "deal_time": 1532931289,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20146,
            "deal_amount": 9999.63,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21313,
            "deal_amount": 2784.47,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21967,
            "deal_amount": 6213.74,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20179,
            "deal_amount": 10082.86,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20018,
            "deal_amount": 3542.76,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20775,
            "deal_amount": 9026.51,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20788,
            "deal_amount": 3154.42,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20635,
            "deal_amount": 5933.73,
            "deal_quantity": 1,
            "deal_time": 1532930653,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21321,
            "deal_amount": 4865.34,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20647,
            "deal_amount": 2493.85,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21707,
            "deal_amount": 3700.57,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21238,
            "deal_amount": 1713.54,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21147,
            "deal_amount": 10333.55,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20871,
            "deal_amount": 513.68,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20878,
            "deal_amount": 1773.38,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21042,
            "deal_amount": 9204.8,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20667,
            "deal_amount": 3281.69,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20523,
            "deal_amount": 3977.95,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20502,
            "deal_amount": 3887.2,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21236,
            "deal_amount": 6102.69,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21249,
            "deal_amount": 6050.82,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20318,
            "deal_amount": 3431.03,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20254,
            "deal_amount": 3783.27,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21693,
            "deal_amount": 4675.34,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20439,
            "deal_amount": 8169.78,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21436,
            "deal_amount": 1109.25,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20959,
            "deal_amount": 5840.34,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21187,
            "deal_amount": 9220.05,
            "deal_quantity": 1,
            "deal_time": 1532930834,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20351,
            "deal_amount": 8293.43,
            "deal_quantity": 1,
            "deal_time": 1532930584,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21916,
            "deal_amount": 2315.9,
            "deal_quantity": 1,
            "deal_time": 1532931289,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21881,
            "deal_amount": 6400.44,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20044,
            "deal_amount": 3402.64,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21543,
            "deal_amount": 2573.28,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21819,
            "deal_amount": 9378.13,
            "deal_quantity": 1,
            "deal_time": 1532931269,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21229,
            "deal_amount": 5210.5,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20524,
            "deal_amount": 9984.07,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20705,
            "deal_amount": 3908.2,
            "deal_quantity": 1,
            "deal_time": 1532930659,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21405,
            "deal_amount": 481.81,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21430,
            "deal_amount": 2663.87,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21462,
            "deal_amount": 1549.35,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20863,
            "deal_amount": 7353.88,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21518,
            "deal_amount": 4526.44,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20335,
            "deal_amount": 3467.45,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21371,
            "deal_amount": 2634.36,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21174,
            "deal_amount": 6041.49,
            "deal_quantity": 1,
            "deal_time": 1532930834,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20945,
            "deal_amount": 1822.89,
            "deal_quantity": 1,
            "deal_time": 1532930759,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21125,
            "deal_amount": 4161.34,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21572,
            "deal_amount": 6686.95,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21118,
            "deal_amount": 2588.36,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20136,
            "deal_amount": 7517.75,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20371,
            "deal_amount": 3450.83,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20708,
            "deal_amount": 3097.25,
            "deal_quantity": 1,
            "deal_time": 1532930659,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21357,
            "deal_amount": 1139.21,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20115,
            "deal_amount": 3760.62,
            "deal_quantity": 1,
            "deal_time": 1532930514,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20431,
            "deal_amount": 8273.37,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20142,
            "deal_amount": 2513.9,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20709,
            "deal_amount": 6213.12,
            "deal_quantity": 1,
            "deal_time": 1532930659,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20025,
            "deal_amount": 1845.88,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20758,
            "deal_amount": 8994.59,
            "deal_quantity": 1,
            "deal_time": 1532930661,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21758,
            "deal_amount": 2698.99,
            "deal_quantity": 1,
            "deal_time": 1532930993,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20357,
            "deal_amount": 1388.61,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21159,
            "deal_amount": 4298.15,
            "deal_quantity": 1,
            "deal_time": 1532930827,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20483,
            "deal_amount": 3868.95,
            "deal_quantity": 1,
            "deal_time": 1532930649,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21892,
            "deal_amount": 9143.57,
            "deal_quantity": 1,
            "deal_time": 1532931273,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21549,
            "deal_amount": 4383.28,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21477,
            "deal_amount": 2476.45,
            "deal_quantity": 1,
            "deal_time": 1532930975,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21128,
            "deal_amount": 5396.58,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20182,
            "deal_amount": 7267.52,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20038,
            "deal_amount": 3501.75,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21005,
            "deal_amount": 1145.12,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21055,
            "deal_amount": 4113.79,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21243,
            "deal_amount": 477.18,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20399,
            "deal_amount": 10152.12,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20156,
            "deal_amount": 398.26,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20664,
            "deal_amount": 1236.48,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21367,
            "deal_amount": 2706.75,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21923,
            "deal_amount": 6493.89,
            "deal_quantity": 1,
            "deal_time": 1532931289,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20379,
            "deal_amount": 3853.89,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20127,
            "deal_amount": 2520.69,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20913,
            "deal_amount": 3891.09,
            "deal_quantity": 1,
            "deal_time": 1532930749,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21683,
            "deal_amount": 1425.09,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20497,
            "deal_amount": 7331.4,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20857,
            "deal_amount": 6276.51,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20320,
            "deal_amount": 1713.13,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20099,
            "deal_amount": 400.11,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20750,
            "deal_amount": 6028.73,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20499,
            "deal_amount": 3406.69,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21293,
            "deal_amount": 2814.36,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21406,
            "deal_amount": 1572.45,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20449,
            "deal_amount": 2384.25,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21035,
            "deal_amount": 6106.25,
            "deal_quantity": 1,
            "deal_time": 1532930790,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21093,
            "deal_amount": 486.07,
            "deal_quantity": 1,
            "deal_time": 1532930820,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20832,
            "deal_amount": 6291.62,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20805,
            "deal_amount": 3833.7,
            "deal_quantity": 1,
            "deal_time": 1532930663,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20601,
            "deal_amount": 2470.33,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 22000,
            "deal_amount": 1099.1,
            "deal_quantity": 1,
            "deal_time": 1532933900,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21992,
            "deal_amount": 3190.02,
            "deal_quantity": 1,
            "deal_time": 1532933900,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20777,
            "deal_amount": 2533.35,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21419,
            "deal_amount": 4836.41,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21527,
            "deal_amount": 6723.24,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20016,
            "deal_amount": 2441.94,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20813,
            "deal_amount": 8857.77,
            "deal_quantity": 1,
            "deal_time": 1532930678,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20252,
            "deal_amount": 10393.42,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21715,
            "deal_amount": 1369.69,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20742,
            "deal_amount": 3856.98,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21424,
            "deal_amount": 2585.85,
            "deal_quantity": 1,
            "deal_time": 1532930840,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20222,
            "deal_amount": 421.79,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20493,
            "deal_amount": 10121.35,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21670,
            "deal_amount": 9432.65,
            "deal_quantity": 1,
            "deal_time": 1532930985,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20798,
            "deal_amount": 494.38,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21254,
            "deal_amount": 2477.22,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21451,
            "deal_amount": 9596.14,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20815,
            "deal_amount": 2499.18,
            "deal_quantity": 1,
            "deal_time": 1532930678,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20060,
            "deal_amount": 408.06,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21456,
            "deal_amount": 1534.85,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20123,
            "deal_amount": 1396.55,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20920,
            "deal_amount": 3045.11,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20004,
            "deal_amount": 9960.38,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20786,
            "deal_amount": 492.52,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21013,
            "deal_amount": 6152.45,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21032,
            "deal_amount": 3973.74,
            "deal_quantity": 1,
            "deal_time": 1532930790,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21921,
            "deal_amount": 2442.02,
            "deal_quantity": 1,
            "deal_time": 1532931289,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20844,
            "deal_amount": 502.66,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20971,
            "deal_amount": 4005.09,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20656,
            "deal_amount": 2444.12,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20744,
            "deal_amount": 3226.93,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20009,
            "deal_amount": 1437.6,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20620,
            "deal_amount": 3994.7,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21108,
            "deal_amount": 5810.41,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20860,
            "deal_amount": 3121.68,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21528,
            "deal_amount": 6785.84,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21172,
            "deal_amount": 10107.86,
            "deal_quantity": 1,
            "deal_time": 1532930834,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21428,
            "deal_amount": 6379.63,
            "deal_quantity": 1,
            "deal_time": 1532930840,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21260,
            "deal_amount": 5125.34,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20715,
            "deal_amount": 3881.76,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21368,
            "deal_amount": 1148.41,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20836,
            "deal_amount": 2544.35,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20481,
            "deal_amount": 5910.97,
            "deal_quantity": 1,
            "deal_time": 1532930649,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20198,
            "deal_amount": 3764.24,
            "deal_quantity": 1,
            "deal_time": 1532930526,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21492,
            "deal_amount": 1539.7,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21234,
            "deal_amount": 4252.1,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21463,
            "deal_amount": 1548.33,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21337,
            "deal_amount": 1123.45,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20398,
            "deal_amount": 2412.72,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21519,
            "deal_amount": 2767.39,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21392,
            "deal_amount": 1119.32,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20534,
            "deal_amount": 7091.41,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21008,
            "deal_amount": 6149.72,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20266,
            "deal_amount": 7567.14,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21510,
            "deal_amount": 1504.68,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20684,
            "deal_amount": 10267.85,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21509,
            "deal_amount": 9453.5,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21650,
            "deal_amount": 3909.5,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20846,
            "deal_amount": 504.72,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20207,
            "deal_amount": 9011.44,
            "deal_quantity": 1,
            "deal_time": 1532930527,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20294,
            "deal_amount": 10311.54,
            "deal_quantity": 1,
            "deal_time": 1532930573,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20509,
            "deal_amount": 3930.66,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21984,
            "deal_amount": 6098.87,
            "deal_quantity": 1,
            "deal_time": 1532933900,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20963,
            "deal_amount": 6983.25,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21412,
            "deal_amount": 6318.94,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20689,
            "deal_amount": 3961.3,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21078,
            "deal_amount": 5924.24,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20726,
            "deal_amount": 1235.99,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20326,
            "deal_amount": 3819.46,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20607,
            "deal_amount": 1626.26,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21781,
            "deal_amount": 2727.31,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21239,
            "deal_amount": 4222.64,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21331,
            "deal_amount": 1609.01,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20346,
            "deal_amount": 7749.45,
            "deal_quantity": 1,
            "deal_time": 1532930584,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21589,
            "deal_amount": 4522.01,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21676,
            "deal_amount": 9030.21,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20501,
            "deal_amount": 5980.51,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20996,
            "deal_amount": 5803.47,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20948,
            "deal_amount": 5888.26,
            "deal_quantity": 1,
            "deal_time": 1532930760,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21482,
            "deal_amount": 4685.46,
            "deal_quantity": 1,
            "deal_time": 1532930975,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21114,
            "deal_amount": 5877.75,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21102,
            "deal_amount": 1711.12,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20535,
            "deal_amount": 8544.58,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20946,
            "deal_amount": 487.15,
            "deal_quantity": 1,
            "deal_time": 1532930759,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20568,
            "deal_amount": 1323.65,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21143,
            "deal_amount": 2735.1,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20071,
            "deal_amount": 2553.5,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21665,
            "deal_amount": 6357.94,
            "deal_quantity": 1,
            "deal_time": 1532930985,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21410,
            "deal_amount": 6256.41,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21814,
            "deal_amount": 9594.52,
            "deal_quantity": 1,
            "deal_time": 1532931269,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20642,
            "deal_amount": 5879.09,
            "deal_quantity": 1,
            "deal_time": 1532930654,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21183,
            "deal_amount": 2741.78,
            "deal_quantity": 1,
            "deal_time": 1532930834,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20730,
            "deal_amount": 3372.27,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20303,
            "deal_amount": 7685.18,
            "deal_quantity": 1,
            "deal_time": 1532930573,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21790,
            "deal_amount": 523.08,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20003,
            "deal_amount": 402.61,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20488,
            "deal_amount": 3223.74,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20763,
            "deal_amount": 6117.9,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21197,
            "deal_amount": 1708.66,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20840,
            "deal_amount": 3083.86,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21806,
            "deal_amount": 6286.34,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20096,
            "deal_amount": 3698.03,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21793,
            "deal_amount": 3296.62,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21049,
            "deal_amount": 6843.46,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20740,
            "deal_amount": 3301.63,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21767,
            "deal_amount": 9842.55,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20816,
            "deal_amount": 3808.71,
            "deal_quantity": 1,
            "deal_time": 1532930680,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21669,
            "deal_amount": 1087.53,
            "deal_quantity": 1,
            "deal_time": 1532930985,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20824,
            "deal_amount": 3800.39,
            "deal_quantity": 1,
            "deal_time": 1532930687,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20308,
            "deal_amount": 2446.21,
            "deal_quantity": 1,
            "deal_time": 1532930582,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21204,
            "deal_amount": 499.24,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21770,
            "deal_amount": 6490.89,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20555,
            "deal_amount": 3998.04,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20240,
            "deal_amount": 5683.49,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21894,
            "deal_amount": 1320.39,
            "deal_quantity": 1,
            "deal_time": 1532931273,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21998,
            "deal_amount": 3164.46,
            "deal_quantity": 1,
            "deal_time": 1532933900,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20052,
            "deal_amount": 1759.08,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20641,
            "deal_amount": 1282.33,
            "deal_quantity": 1,
            "deal_time": 1532930654,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21646,
            "deal_amount": 6248.12,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21811,
            "deal_amount": 2448.3,
            "deal_quantity": 1,
            "deal_time": 1532931269,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21678,
            "deal_amount": 9514.98,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20877,
            "deal_amount": 1145.06,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21941,
            "deal_amount": 1244.33,
            "deal_quantity": 1,
            "deal_time": 1532931290,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21661,
            "deal_amount": 2550.01,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21684,
            "deal_amount": 6287.09,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21873,
            "deal_amount": 6473.47,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21966,
            "deal_amount": 2333.07,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20177,
            "deal_amount": 406.13,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20518,
            "deal_amount": 3280.61,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21421,
            "deal_amount": 2631.49,
            "deal_quantity": 1,
            "deal_time": 1532930840,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20942,
            "deal_amount": 1157.31,
            "deal_quantity": 1,
            "deal_time": 1532930759,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21573,
            "deal_amount": 9308.23,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21949,
            "deal_amount": 2702.25,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21489,
            "deal_amount": 2799.02,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21439,
            "deal_amount": 2510.31,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21567,
            "deal_amount": 6503.36,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20757,
            "deal_amount": 3208.58,
            "deal_quantity": 1,
            "deal_time": 1532930661,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20939,
            "deal_amount": 1798.63,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20621,
            "deal_amount": 3968.9,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21761,
            "deal_amount": 2726.84,
            "deal_quantity": 1,
            "deal_time": 1532930993,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20473,
            "deal_amount": 1348.03,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20768,
            "deal_amount": 5967.29,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20014,
            "deal_amount": 3495.89,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21584,
            "deal_amount": 4166.4,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20110,
            "deal_amount": 7594.01,
            "deal_quantity": 1,
            "deal_time": 1532930514,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21322,
            "deal_amount": 4439.44,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20935,
            "deal_amount": 2931.91,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21541,
            "deal_amount": 9018.86,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20967,
            "deal_amount": 483.59,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21800,
            "deal_amount": 4666.33,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21526,
            "deal_amount": 9627.25,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21908,
            "deal_amount": 1096.08,
            "deal_quantity": 1,
            "deal_time": 1532931273,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20045,
            "deal_amount": 1400.44,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21571,
            "deal_amount": 9425.41,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20617,
            "deal_amount": 3078.25,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21809,
            "deal_amount": 4723.95,
            "deal_quantity": 1,
            "deal_time": 1532931269,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21047,
            "deal_amount": 9302.92,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20869,
            "deal_amount": 9015.33,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20396,
            "deal_amount": 8124.61,
            "deal_quantity": 1,
            "deal_time": 1532930588,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20186,
            "deal_amount": 10169.41,
            "deal_quantity": 1,
            "deal_time": 1532930526,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21939,
            "deal_amount": 1105.66,
            "deal_quantity": 1,
            "deal_time": 1532931290,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20807,
            "deal_amount": 8939.84,
            "deal_quantity": 1,
            "deal_time": 1532930663,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21771,
            "deal_amount": 3359.63,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20476,
            "deal_amount": 3421.75,
            "deal_quantity": 1,
            "deal_time": 1532930648,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20560,
            "deal_amount": 10040.96,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21585,
            "deal_amount": 2766.78,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20289,
            "deal_amount": 1689.9,
            "deal_quantity": 1,
            "deal_time": 1532930573,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20663,
            "deal_amount": 1258.22,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21280,
            "deal_amount": 6939.99,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20402,
            "deal_amount": 5991.68,
            "deal_quantity": 1,
            "deal_time": 1532930595,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21284,
            "deal_amount": 5060.94,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20933,
            "deal_amount": 3988.51,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21259,
            "deal_amount": 473.06,
            "deal_quantity": 1,
            "deal_time": 1532930836,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20316,
            "deal_amount": 1695.87,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21973,
            "deal_amount": 513.71,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20943,
            "deal_amount": 1811.2,
            "deal_quantity": 1,
            "deal_time": 1532930759,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21971,
            "deal_amount": 6160.22,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21135,
            "deal_amount": 4178.23,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21310,
            "deal_amount": 4941.6,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21363,
            "deal_amount": 9818.85,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21124,
            "deal_amount": 1691.96,
            "deal_quantity": 1,
            "deal_time": 1532930826,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20471,
            "deal_amount": 3253.9,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21914,
            "deal_amount": 8668.63,
            "deal_quantity": 1,
            "deal_time": 1532931283,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21394,
            "deal_amount": 2718.41,
            "deal_quantity": 1,
            "deal_time": 1532930839,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21175,
            "deal_amount": 9987.15,
            "deal_quantity": 1,
            "deal_time": 1532930834,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21841,
            "deal_amount": 6482.63,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20188,
            "deal_amount": 9220.6,
            "deal_quantity": 1,
            "deal_time": 1532930526,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21098,
            "deal_amount": 10274.75,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21674,
            "deal_amount": 9123.29,
            "deal_quantity": 1,
            "deal_time": 1532930988,
            "deal_type": "B",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20625,
            "deal_amount": 5879.47,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21348,
            "deal_amount": 1607.76,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20145,
            "deal_amount": 2539.72,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21465,
            "deal_amount": 6464.63,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20573,
            "deal_amount": 3969.99,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21324,
            "deal_amount": 461.97,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20883,
            "deal_amount": 7470.09,
            "deal_quantity": 1,
            "deal_time": 1532930742,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21445,
            "deal_amount": 2685.34,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20342,
            "deal_amount": 7831.94,
            "deal_quantity": 1,
            "deal_time": 1532930584,
            "deal_type": "B",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21475,
            "deal_amount": 2745.21,
            "deal_quantity": 1,
            "deal_time": 1532930975,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21913,
            "deal_amount": 505.84,
            "deal_quantity": 1,
            "deal_time": 1532931283,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21001,
            "deal_amount": 3071.52,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20243,
            "deal_amount": 1651.92,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21657,
            "deal_amount": 9560.5,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21539,
            "deal_amount": 4477.84,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20504,
            "deal_amount": 1618.21,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21038,
            "deal_amount": 1767.22,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20062,
            "deal_amount": 3522.79,
            "deal_quantity": 1,
            "deal_time": 1532930301,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20313,
            "deal_amount": 8498.16,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21968,
            "deal_amount": 2449.8,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21370,
            "deal_amount": 2669.52,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20661,
            "deal_amount": 3318.8,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20631,
            "deal_amount": 3334.22,
            "deal_quantity": 1,
            "deal_time": 1532930652,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21752,
            "deal_amount": 2522.43,
            "deal_quantity": 1,
            "deal_time": 1532930993,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20361,
            "deal_amount": 10371.73,
            "deal_quantity": 1,
            "deal_time": 1532930585,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20928,
            "deal_amount": 6280.93,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20947,
            "deal_amount": 6221.77,
            "deal_quantity": 1,
            "deal_time": 1532930760,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21155,
            "deal_amount": 9018.45,
            "deal_quantity": 1,
            "deal_time": 1532930827,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20856,
            "deal_amount": 513.08,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21849,
            "deal_amount": 2464.21,
            "deal_quantity": 1,
            "deal_time": 1532931271,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20429,
            "deal_amount": 6157.18,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21206,
            "deal_amount": 1695.39,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20929,
            "deal_amount": 1791.81,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20086,
            "deal_amount": 402.66,
            "deal_quantity": 1,
            "deal_time": 1532930304,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21160,
            "deal_amount": 6757,
            "deal_quantity": 1,
            "deal_time": 1532930827,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20282,
            "deal_amount": 10421.99,
            "deal_quantity": 1,
            "deal_time": 1532930572,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20803,
            "deal_amount": 3065.69,
            "deal_quantity": 1,
            "deal_time": 1532930663,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21481,
            "deal_amount": 2467.06,
            "deal_quantity": 1,
            "deal_time": 1532930975,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20197,
            "deal_amount": 9122.86,
            "deal_quantity": 1,
            "deal_time": 1532930526,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21084,
            "deal_amount": 1190.97,
            "deal_quantity": 1,
            "deal_time": 1532930819,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21011,
            "deal_amount": 1838.6,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21277,
            "deal_amount": 481.23,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21642,
            "deal_amount": 9392.2,
            "deal_quantity": 1,
            "deal_time": 1532930980,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20178,
            "deal_amount": 5718.05,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20669,
            "deal_amount": 2448.75,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21157,
            "deal_amount": 6707.96,
            "deal_quantity": 1,
            "deal_time": 1532930827,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21178,
            "deal_amount": 495.84,
            "deal_quantity": 1,
            "deal_time": 1532930834,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21270,
            "deal_amount": 2697.45,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21739,
            "deal_amount": 8753.31,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20680,
            "deal_amount": 10177.26,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20686,
            "deal_amount": 477.42,
            "deal_quantity": 1,
            "deal_time": 1532930657,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21553,
            "deal_amount": 2796.46,
            "deal_quantity": 1,
            "deal_time": 1532930976,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21446,
            "deal_amount": 9046.94,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21700,
            "deal_amount": 6758.17,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20525,
            "deal_amount": 1607.78,
            "deal_quantity": 1,
            "deal_time": 1532930650,
            "deal_type": "B",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20477,
            "deal_amount": 1650.49,
            "deal_quantity": 1,
            "deal_time": 1532930648,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21077,
            "deal_amount": 10184.34,
            "deal_quantity": 1,
            "deal_time": 1532930818,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21805,
            "deal_amount": 9523.64,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21290,
            "deal_amount": 4289.66,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21677,
            "deal_amount": 2601.74,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21969,
            "deal_amount": 6574.05,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20173,
            "deal_amount": 5667.1,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20443,
            "deal_amount": 6092.19,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21320,
            "deal_amount": 464.75,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "S",
            "instrument": "Floral",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20101,
            "deal_amount": 1419.45,
            "deal_quantity": 1,
            "deal_time": 1532930514,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21117,
            "deal_amount": 2562.03,
            "deal_quantity": 1,
            "deal_time": 1532930824,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21440,
            "deal_amount": 2661.3,
            "deal_quantity": 1,
            "deal_time": 1532930872,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20828,
            "deal_amount": 1190.15,
            "deal_quantity": 1,
            "deal_time": 1532930741,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21200,
            "deal_amount": 4317.92,
            "deal_quantity": 1,
            "deal_time": 1532930835,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20675,
            "deal_amount": 3101.51,
            "deal_quantity": 1,
            "deal_time": 1532930656,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21880,
            "deal_amount": 8663.42,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20323,
            "deal_amount": 10400.47,
            "deal_quantity": 1,
            "deal_time": 1532930583,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20749,
            "deal_amount": 10061.78,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "S",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21165,
            "deal_amount": 2483.11,
            "deal_quantity": 1,
            "deal_time": 1532930827,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21902,
            "deal_amount": 6222.42,
            "deal_quantity": 1,
            "deal_time": 1532931273,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21957,
            "deal_amount": 2354.31,
            "deal_quantity": 1,
            "deal_time": 1532933393,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20121,
            "deal_amount": 8087.75,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "S",
            "instrument": "Deuteronic",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21734,
            "deal_amount": 2451.37,
            "deal_quantity": 1,
            "deal_time": 1532930992,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21885,
            "deal_amount": 2715.49,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20235,
            "deal_amount": 1439.81,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20482,
            "deal_amount": 3383.89,
            "deal_quantity": 1,
            "deal_time": 1532930649,
            "deal_type": "S",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20735,
            "deal_amount": 8895.5,
            "deal_quantity": 1,
            "deal_time": 1532930660,
            "deal_type": "S",
            "instrument": "Heliosphere",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20160,
            "deal_amount": 10091.52,
            "deal_quantity": 1,
            "deal_time": 1532930522,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21884,
            "deal_amount": 1122.54,
            "deal_quantity": 1,
            "deal_time": 1532931272,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20977,
            "deal_amount": 1174.8,
            "deal_quantity": 1,
            "deal_time": 1532930762,
            "deal_type": "B",
            "instrument": "Celestial",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21798,
            "deal_amount": 2378.86,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "S",
            "instrument": "Koronis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20454,
            "deal_amount": 1331.01,
            "deal_quantity": 1,
            "deal_time": 1532930647,
            "deal_type": "S",
            "instrument": "Celestial",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20544,
            "deal_amount": 7950.3,
            "deal_quantity": 1,
            "deal_time": 1532930651,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20919,
            "deal_amount": 10311.63,
            "deal_quantity": 1,
            "deal_time": 1532930758,
            "deal_type": "B",
            "instrument": "Eclipse",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20391,
            "deal_amount": 3296.27,
            "deal_quantity": 1,
            "deal_time": 1532930588,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20434,
            "deal_amount": 2432.78,
            "deal_quantity": 1,
            "deal_time": 1532930598,
            "deal_type": "B",
            "instrument": "Koronis",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21303,
            "deal_amount": 2650.56,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 2,
                "counterparty_name": "Richard",
                "counterparty_date_registered": 1041379200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20020,
            "deal_amount": 3519.81,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "S",
            "instrument": "Jupiter",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20766,
            "deal_amount": 7615.83,
            "deal_quantity": 1,
            "deal_time": 1532930662,
            "deal_type": "S",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20205,
            "deal_amount": 3523.3,
            "deal_quantity": 1,
            "deal_time": 1532930527,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21369,
            "deal_amount": 7061.64,
            "deal_quantity": 1,
            "deal_time": 1532930837,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21898,
            "deal_amount": 1293.45,
            "deal_quantity": 1,
            "deal_time": 1532931273,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21785,
            "deal_amount": 6540.13,
            "deal_quantity": 1,
            "deal_time": 1532931267,
            "deal_type": "B",
            "instrument": "Galactia",
            "counterparty": {
                "id": 3,
                "counterparty_name": "Selvyn",
                "counterparty_date_registered": 1451606400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20388,
            "deal_amount": 3333.03,
            "deal_quantity": 1,
            "deal_time": 1532930588,
            "deal_type": "S",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20138,
            "deal_amount": 1728.07,
            "deal_quantity": 1,
            "deal_time": 1532930515,
            "deal_type": "S",
            "instrument": "Lunatic",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21563,
            "deal_amount": 2516.03,
            "deal_quantity": 1,
            "deal_time": 1532930977,
            "deal_type": "B",
            "instrument": "Interstella",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20021,
            "deal_amount": 5820.25,
            "deal_quantity": 1,
            "deal_time": 1532930300,
            "deal_type": "B",
            "instrument": "Borealis",
            "counterparty": {
                "id": 1,
                "counterparty_name": "Lina",
                "counterparty_date_registered": 1388534400,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 21893,
            "deal_amount": 6214.35,
            "deal_quantity": 1,
            "deal_time": 1532931273,
            "deal_type": "S",
            "instrument": "Borealis",
            "counterparty": {
                "id": 5,
                "counterparty_name": "Estelle",
                "counterparty_date_registered": 978307200,
                "counterparty_status": "A"
            }
        },
        {
            "deal_id": 20228,
            "deal_amount": 3358.72,
            "deal_quantity": 1,
            "deal_time": 1532930571,
            "deal_type": "B",
            "instrument": "Astronomica",
            "counterparty": {
                "id": 4,
                "counterparty_name": "Lewis",
                "counterparty_date_registered": 1230768000,
                "counterparty_status": "A"
            }
        }
    ];