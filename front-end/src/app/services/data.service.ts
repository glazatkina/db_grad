import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  public getDeals() {
    return this.http.get(this.apiUrl + "data/deals");
  }

  public getInstruments() {
    return this.http.get(this.apiUrl + "data/instruments");
  }



}
