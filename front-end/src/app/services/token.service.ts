import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

    private token = '';
    private loggedIn$ = new BehaviorSubject<boolean>(false);

    constructor() { }

    public updateToken(token: string) {
        this.token = token;
        localStorage.setItem('token', token);
    }

    public removeToken() {
        localStorage.removeItem('token');
        this.loggedIn$.next(false);
    }

    public loggedIn() {
        return this.loggedIn$.asObservable();
    }

    public getToken() {
        const token = localStorage.getItem('token');
        if(token && token !== '') {
            return token;
        }
        else {
            return '';
        }
    }
}
