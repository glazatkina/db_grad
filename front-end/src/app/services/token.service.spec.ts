import { TestBed, inject } from '@angular/core/testing';

import { TokenService } from './token.service';

describe('TokenServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TokenServiceService]
    });
  });

  it('should be created', inject([TokenServiceService], (service: TokenServiceService) => {
    expect(service).toBeTruthy();
  }));
});
