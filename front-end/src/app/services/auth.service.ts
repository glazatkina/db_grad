import { Injectable } from '@angular/core';
import {HttpClient, HttpClientModule, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {TokenService} from './token.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

    private auth_url: string = environment.apiUrl;

    constructor(private http: HttpClient,
                private tokenService: TokenService) {}

    public login(name: string, password: string): Observable<any> {
        return this.http.post(this.auth_url + 'login/', {name: name, password: password})
            .pipe(map(result => {
                if (result) {
                    const token: string = result['token'];
                    if(token && token !== '') {
                        this.tokenService.updateToken(token);
                        return token;
                    }
                }
            }))
    }

    public register(first: string, last: string, email: string, password: string): Observable<any> {
        return this.http.post(this.auth_url + 'register/', {first: first, last: last, email: email, password: password})
            .pipe(map(result => {

            }))
    }

    public checkToken() {
        const headers = new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('token')}`);
        return this.http.get(this.auth_url + 'login/', {headers})
    }

    public logout() {
        this.tokenService.removeToken();
    }

    private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            console.error(error); // log to console instead
            console.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}
