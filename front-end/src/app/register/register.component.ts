import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

    public registerForm: FormGroup;

    constructor(private auth: AuthService,
                private formBuilder: FormBuilder,
                private router: Router) { }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            first: ['', Validators.required],
            last: ['', Validators.required],
            email: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    get f() { return this.registerForm.controls; }

    submitForm() {
        console.log(this.f.email.value);
        console.log(this.f.password.value);
        if(this.registerForm.invalid) {
            return;
        }

        this.auth.register(this.f.first.value, this.f.last.value, this.f.email.value, this.f.password.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate(['/login']);
                },
                error => {
                    console.log(error);
                }
            )
    }

}
