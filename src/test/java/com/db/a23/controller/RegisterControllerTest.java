package com.db.a23.controller;

import com.db.a23.TestBase;
import org.junit.Ignore;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

public class RegisterControllerTest extends TestBase {



    @Test
    @Ignore
    public void testRegisterUserWithAlreadyUsedUsername() {
        String userJson =
                "{" +
                        "\"email\": \"test@mail.com\"" +
                        "\"first\": \"David\"" +
                        "\"last\": \"Sands\"" +
                        "\"password\": \"password\"" +
                        "}";

        Response r = target.path("register").request().post(Entity.json(userJson));
        assertEquals(Response.Status.UNAUTHORIZED.getStatusCode(), r.getStatus());
    }
    @Test
    @Ignore
    public void testRegisterNewUserWithValidDetails() {
        String userJson =
                "{" +
                        "\"email\": \"newUser@mail.com\"," +
                        "\"first\": \"David\"," +
                        "\"last\": \"Sands\"," +
                        "\"password\": \"password\"" +
                "}";

        Response r = target.path("register").request().post(Entity.json(userJson));
        assertEquals(Response.Status.CREATED.getStatusCode(), r.getStatus());
    }




    @Test
    @Ignore
    public void testRegisterUserWithMissingFields() {
        String userJson =
                "{" +
                        "\"email\": \"newUser@mail.com\"," +
                        "\"first\": \"\"," +
                        "\"last\": \"\"," +
                        "\"password\": \"password\"" +
                        "}";

        Response r = target.path("register").request().post(Entity.json(userJson));
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), r.getStatus());
    }

    @Test
    @Ignore
    public void testRegisterUserWithSQLInjectionString() {
        String userJson =
                "{" +
                        "\"email\": \"name'); DROP TABLE Users; --\"," +
                        "\"first\": \"test\"," +
                        "\"last\": \"user\"," +
                        "\"password\": \"password\"" +
                "}";

        Response r = target.path("register").request().post(Entity.json(userJson));
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), r.getStatus());
    }

}
