package com.db.a23.controller;

import com.db.a23.TestBase;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;


import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import static org.junit.Assert.*;

//import javax.ws.rs.client.Entity;
//import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

public class GlueLoginControllerTest extends TestBase{

    @Given("^The correct values are input$")
    public void the_correct_values_are_input() throws Throwable {
        String userJSON =
                "{" +
                        "\"email\": \"test@mail.com\"," +
                        "\"password\": \"password\"" +
                        "}";

    }

    @Then("^login should be successful$")
    public void login_should_be_successful() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        String userJSON =
                "{" +
                        "\"email\": \"test@mail.com\"," +
                        "\"password\": \"password\"" +
                        "}";
        Response r = target.path("login").request().post(Entity.json(userJSON));
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
    }
}
