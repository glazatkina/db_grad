package com.db.a23.controller;

import com.db.a23.TestBase;
import com.db.a23.dao.UserDAO;
import com.db.a23.dao.impl.UserDAOImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Ignore;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class LoginControllerTest extends TestBase {

    private UserDAO ud = new UserDAOImpl();
    private ObjectMapper mapper = new ObjectMapper();

    @Test
    @Ignore
    public void testLoginWithCorrectValues() throws IOException {
        String userJSON =
                "{" +
                        "\"name\": \"alison\"," +
                        "\"password\": \"gradprog2016@07\"" +
                "}";

        Response r = target.path("login").request().post(Entity.json(userJSON));

        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());

        String token = mapper.readTree(r.readEntity(String.class)).get("token").asText();
        assertEquals(token, LoginController.createAuthToken("alison"));
    }

    @Test
    public void testLoginWithWrongPassword() {
        String userJSON =
                "{" +
                        "\"name\": \"test@mail.com\"," +
                        "\"password\": \"badPassword\"" +
                "}";

        Response r = target.path("login").request().post(Entity.json(userJSON));

        assertEquals(Response.Status.UNAUTHORIZED.getStatusCode(), r.getStatus());
    }

    @Test
    public void testLoginWithIncorrectEmail() {
        String userJSON =
                "{" +
                    "\"name\": \"fake@mail.com\"," +
                    "\"password\": \"password\"" +
                "}";

        Response r = target.path("login").request().post(Entity.json(userJSON));

        assertEquals(Response.Status.UNAUTHORIZED.getStatusCode(), r.getStatus());
    }

    @Test
    public void testLoginWhenNoPasswordIsProvided() {
        String userJSON =
                "{" +
                        "\"name\": \"test@mail.com\"," +
                        "\"password\": \"\"" +
                "}";

        Response r = target.path("login").request().post(Entity.json(userJSON));

        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), r.getStatus());
    }

    @Test
    public void testLoginWhenNoEmailProvided() {
        String userJson =
                "{" +
                        "\"name\": \"\"," +
                        "\"password\": \"password\"" +
                "}";

        Response r = target.path("login").request().post(Entity.json(userJson));

        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), r.getStatus());
    }

}
