package com.db.a23.controller;


import com.db.a23.TestBase;
import org.junit.Ignore;
import org.junit.Test;

import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

public class DataControllerTest extends TestBase {

    @Test
    public void testGetDeals() {
        String token = "fakeToken";
        Response r = target.path("data/deals").request().header("Authorization", "Bearer "+token).get();
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
    }

    @Test
    public void testGetInstruments() {
        Response r = target.path("data/instruments").request().get();
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
    }

    @Test
    @Ignore
    public void testGetDealsById() {
        Response r = target.path("data/deals/fakeId").request().get();
        assertEquals(Response.Status.OK.getStatusCode(), r.getStatus());
    }
}
