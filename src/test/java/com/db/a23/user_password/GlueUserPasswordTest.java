package com.db.a23.user_password;

import com.db.a23.beans.User;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.*;
public class GlueUserPasswordTest {
    private int userId = 95;
    private String firstName = "freddy";
    private String lastName= "caceres";
    private String email = "facs95@gmail.com";
    private String password = "password";


    private User user;

    @Given("^System Takes users data$")
    public void system_Takes_users_data() throws Throwable {
        user = new User(userId,
                firstName,
                lastName,
                email,
                password);
    }

    @When("^Ask for it in the login page$")
    public void ask_for_it_in_the_login_page() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
    }


    @Then("^Users information should be the same as the data given$")
    public void users_information_should_be_the_same_as_the_data_given() throws Throwable {
        assertEquals(userId, user.getUserId());
        assertEquals(firstName, user.getFirstName());
        assertEquals(lastName, user.getLastName());
        assertEquals(email, user.getEmail());
    }

    @Given("^Password was input to the system$")
    public void password_was_input_to_the_system() throws Throwable {
        user = new User(userId,
                firstName,
                lastName,
                email,
                password);
    }

    @Then("^The password should be different to the one input$")
    public void the_password_should_be_different_to_the_one_input() throws Throwable {
        assertNotEquals(password, user.getPassword());
    }

}
