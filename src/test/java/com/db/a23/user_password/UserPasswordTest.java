package com.db.a23.user_password;

import com.db.a23.beans.User;
import org.junit.Test;



import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class UserPasswordTest {
    private int userId = 95;
    private String firstName = "freddy";
    private String lastName= "caceres";
    private String email = "facs95@gmail.com";
    private String password = "password";


    private User user;
    @Test
    public void testGettersAndSetters (){
        user = new User(userId,
        firstName,
        lastName,
        email,
        password);

        assertEquals(userId, user.getUserId());
        assertEquals(firstName, user.getFirstName());
        assertEquals(lastName, user.getLastName());
        assertEquals(email, user.getEmail());
    }




}
