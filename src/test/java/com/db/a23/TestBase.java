package com.db.a23;

import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public abstract class TestBase {

    protected static HttpServer server;
    protected static WebTarget target;

    @BeforeClass
    public static void setUp() {
        server = TestServer.startServer();
        Client c = ClientBuilder.newClient();
        target = c.target(TestServer.TEST_URL + TestServer.TEST_API_BASE);
    }

    @AfterClass
    public static void shutDown() {
        server.shutdown();
    }
}
