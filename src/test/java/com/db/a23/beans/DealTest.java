package com.db.a23.beans;
import com.db.a23.beans.Deal;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import static org.junit.Assert.assertEquals;
public class DealTest {
    private int id = 1000;
    private ZonedDateTime dealTime= ZonedDateTime.parse("2007-12-03T10:15:30+01:00[Europe/Paris]");
    private BigDecimal dealAmount = new BigDecimal("0.7777");
    private long dealQuantity = 7777;
    private DealType dealType;
    private Instrument instrument;
    private Counterparty counterparty;
    private Deal deal = new Deal(id, dealTime, dealAmount, dealQuantity, dealType, instrument, counterparty);

    @Test
    public void DealSetterTest(){
        deal.setId(id);
        deal.setCounterparty(counterparty);
        deal.setDealAmount(dealAmount);
        deal.setDealQuantity(dealQuantity);
        deal.setDealTime(dealTime);
        deal.setInstrument(instrument);
    }

    @Test
    public void dealGettersTest () {
        assertEquals(deal.getCounterparty(), counterparty);
        assertEquals(deal.getDealAmount(), dealAmount);
        assertEquals(deal.getDealQuantity(), dealQuantity);
        assertEquals(deal.getDealTime(), dealTime);
        assertEquals(deal.getId(),id);
        assertEquals(deal.getInstrument(), instrument);
    }

    @Test
    public void dealToStringTest(){
        String expectedString = "Deal{" +
                "id=" + id +
                ", dealTime=" + dealTime +
                ", dealAmount=" + dealAmount +
                ", dealQuantity=" + dealQuantity +
                ", dealType=" + dealType +
                ", instrument=" + instrument +
                ", counterparty=" + counterparty +
                '}';

        assertEquals(deal.toString(), expectedString);
    }


}
