package com.db.a23.beans;
import com.db.a23.beans.Counterparty;
import org.junit.Test;
import com.fasterxml.jackson.annotation.JsonGetter;
import java.time.ZoneId;
import static org.junit.Assert.*;
import java.time.ZonedDateTime;
public class CounterPartyTest {
    private long id = 7;
    private String counterpartyName = "counterPartyName";
    private ZonedDateTime counterpartyDateRegistered = ZonedDateTime.parse("2007-12-03T10:15:30+01:00[Europe/Paris]");
    private CounterpartyStatus counterpartyStatus;
    private Counterparty counterParty = new Counterparty(id, counterpartyName, counterpartyDateRegistered, counterpartyStatus);
    @Test
    public void counterPartySetterandGetterTest(){
        counterParty.setId(id);
        counterParty.setCounterpartyDateRegistered(counterpartyDateRegistered);
        counterParty.setCounterpartyStatus(counterpartyStatus);
        counterParty.setCounterpartyName(counterpartyName);

        assertEquals(id, counterParty.getId() );
    }

    @Test
    public void counterPartyToStringTest(){
        String returnString = counterParty.toString();
        String expectedString = "Counterparty{" +
                "id=" + id +
                ", counterpartyName='" + counterpartyName + '\'' +
                ", counterpartyDateRegistered=" + counterpartyDateRegistered +
                ", counterpartyStatus=" + counterpartyStatus +
                '}';
        assertEquals(returnString, expectedString );
    }


}
