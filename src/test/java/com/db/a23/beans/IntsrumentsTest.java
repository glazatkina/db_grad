package com.db.a23.beans;
import org.junit.Test;
import static org.junit.Assert.*;
import com.db.a23.beans.Instrument;

public class IntsrumentsTest {
    private int id = 7;
    private String instrumentName = "InstrumentName";
    private Instrument instrument = new Instrument(id, instrumentName);

    @Test
    public void instrumentsSettersTest (){
        instrument.setInstrumentName((instrumentName));
        instrument.setId(id);
    }

    @Test
    public void instrumentsGettersTest () {
        long returnId = instrument.getId();
        String returnInstrumentName = instrument.getInstrumentName();

        assertEquals(id, returnId);
        assertEquals(instrumentName, returnInstrumentName);
    }

    @Test
    public void toStringTest(){
        String returnString = instrument.toString();
        assertEquals(returnString, "Instrument{" +
                "id=" + id +
                ", instrumentName='" + instrumentName + '\'' +
                '}');
    }
    @Test
    public void hashTest(){
        int hashReturn = instrument.hashCode();

        assertNotEquals(hashReturn, id);
        assertNotEquals(hashReturn, instrumentName);

    }
}
