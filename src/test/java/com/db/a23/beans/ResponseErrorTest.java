package com.db.a23.beans;
import org.junit.Test;
import static org.junit.Assert.*;
import com.db.a23.controller.ResponseError;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;

import javax.json.Json;

public class ResponseErrorTest {
    private String message = "Error";
    private ResponseError response = new ResponseError(message);
    private String newMessage = "New Error";
    @Test
    public void getErrorTest (){
        String returnError = response.getError();
        assertEquals(message, returnError);
    }

    @Test
    public void setErrorTest(){

        response.setError(newMessage);
        String returnError = response.getError();
        assertEquals(newMessage, returnError);
    }

    @Test
    public void toJsonPassedTest(){
        String jsonReturned  = response.toJson();
        String newReturn = "{\"error\":\"" + message + "\"}";
        assertEquals(jsonReturned, newReturn );
    }

  
}
