package com.db.a23;

import com.db.a23.auth.AuthenticationFilter;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.net.URI;

public class TestServer {

    static final String TEST_URL = "http://localhost:5050";
    static final String TEST_API_BASE = "/api/";

    public static HttpServer startServer() {
        final ResourceConfig rc = new ResourceConfig()
                .register(AuthenticationFilter.class)
                .packages("com.db.a23.controller");
        //.register(AuthenticationFilter.class);
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(TEST_URL + TEST_API_BASE), rc);
    }
}
