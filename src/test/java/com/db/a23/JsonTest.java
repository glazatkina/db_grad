package com.db.a23;

import com.db.a23.beans.Deal;
import com.db.a23.controller.DataController;
import com.db.a23.dao.impl.DealDAOImpl;
import com.db.a23.json.DealSerialiser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.junit.Test;

import java.util.Collection;

import static org.junit.Assert.*;

public class JsonTest {

    private ObjectMapper mapper;
    public JsonTest() {
        mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(Deal.class, new DealSerialiser());
        mapper.registerModule(module);
    }

    @Test
    public void testSerialiseDeals() throws JsonProcessingException {
        Collection<Deal> deals = new DealDAOImpl().getAllDeals();
        assertNotNull(deals);

        String json = mapper.writeValueAsString(deals);
        assertNotEquals("", json);
    }
}
