package com.db.a23.database;

import com.db.a23.TestBase;
import com.db.a23.repository.ConnectionUtils;
import org.junit.Ignore;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class DatabaseConnectionTest extends TestBase {

    @Test
    @Ignore
    public void correctConnectionTest() {
        Connection connection;

        try {
            connection = ConnectionUtils.getConnection();
            assertEquals(false, connection.isClosed() );
        } catch (SQLException e) {
            fail(e.getMessage());
        } catch (ClassNotFoundException e) {
            fail(e.getMessage());
        }
    }

}
