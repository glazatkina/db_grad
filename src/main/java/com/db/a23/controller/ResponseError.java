package com.db.a23.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ResponseError {
    private String error;
    private ObjectMapper om = new ObjectMapper();

    /**
     * Create a new response error with a given message.
     * @param message The message to give the response error.
     */
    public ResponseError(final String message) {
        this.error = message;
    }

    public String toJson() {
        try {
            return om.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return "{'error':'json parse error'}";
        }
    }

    public String getError() {
        return error;
    }

    public void setError(final String error) {
        this.error = error;
    }
}
