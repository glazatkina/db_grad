package com.db.a23.controller;

import com.db.a23.beans.Session;
import com.db.a23.beans.User;
import com.db.a23.dao.SessionDAO;
import com.db.a23.dao.UserDAO;
import com.db.a23.dao.impl.SessionDAOImpl;
import com.db.a23.dao.impl.UserDAOImpl;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**#
 *
 * @author louis
 * Login Controller, responsible for handling User login requests.
 * This requires a correct Username and Password or else login will
 * fail with a status code.
 */
@Path("login")
public class LoginController {

    private ObjectMapper objectMapper;
    private UserDAO userDao;
    private SessionDAO sessionDao;

    public LoginController() {
        objectMapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(UserJson.class, new UserJsonDeserializer());
        objectMapper.registerModule(module);
        userDao = new UserDAOImpl();
        sessionDao = new SessionDAOImpl();
    }


    @POST
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response loginUser(final String userJson) {
        try {
            UserJson deserialisedUser = objectMapper.readValue(userJson, UserJson.class);

            // TODO: When database interaction is done, need to change this to fetch user from DB and validate password.
            String password = deserialisedUser.getPassword();
            String email = deserialisedUser.getName();

            if(email.equalsIgnoreCase("")) {
                String response = new ResponseError("Missing name.").toJson();
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }
            if(password.equalsIgnoreCase("")) {
                String response = new ResponseError("Missing password.").toJson();
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }


            User expected = userDao.getUserByEmail(email);
            if(expected != null) {
                if(expected.getPassword().equals(password)) {
                    final String token = createAuthToken(deserialisedUser.getName());
                    sessionDao.addSession(new Session(0, expected, token));
                    final String response = "{\"token\": \"" + token + "\"}";
                    return Response.status(Response.Status.OK).entity(response).build(); // TODO: Check user and return token.
                }
                else {
                    String response = new ResponseError("Incorrect password").toJson();
                    return Response.status(Response.Status.UNAUTHORIZED).entity(response).build();
                }
            }
            else {
                return Response.status(Response.Status.UNAUTHORIZED).entity(new ResponseError("Email not found.").toJson()).build();
            }
        } catch (IOException e) {
            String resp = new ResponseError("Invalid data format").toJson();
            return Response.status(Response.Status.BAD_REQUEST).entity(resp).build();
        }
    }

    @DELETE
    @Path("")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response logout() {
        return Response.status(Response.Status.OK).build();
    }

    public static String createAuthToken(String email) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedhash = digest.digest(
                    email.getBytes(StandardCharsets.UTF_8));
            return bytesToHex(encodedhash);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return email;

//        return Hashing.sha512().hashString(name, StandardCharsets.UTF_8).toString();
    }


    private class UserJson {
        private String name;
        private String password;

        public UserJson(String name, String password) {
            this.name = name;
            this.password = password;
        }

        public String getPassword() {
            return password;
        }

        public String getName() {
            return name;
        }
    }

    private class UserJsonDeserializer extends StdDeserializer<UserJson> {

        protected UserJsonDeserializer() {
            this(null);
        }

        protected UserJsonDeserializer(Class<?> vc) {
            super(vc);
        }

        @Override
        public UserJson deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            JsonNode node = jsonParser.getCodec().readTree(jsonParser);
            String email = node.get("name").asText();
            String password = node.get("password").asText();
            return new UserJson(email, password);
        }
    }

    private static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
}