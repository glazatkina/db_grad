package com.db.a23.controller;


import com.db.a23.beans.Deal;
import com.db.a23.beans.User;
import com.db.a23.dao.UserDAO;
import com.db.a23.dao.impl.UserDAOImpl;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Path("/register")
public class RegisterController {

    private ObjectMapper objectMapper;
    private UserDAO userDao;

    public RegisterController() {
        objectMapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(UserRegistration.class, new UserRegDeserialiser());
        objectMapper.registerModule(module);
        userDao = new UserDAOImpl();
    }

    @POST
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response registerNewUser(String userJson) {
        try {
            UserRegistration userReg = objectMapper.readValue(userJson, UserRegistration.class);
            String email = userReg.getEmail();
            String pass = userReg.getPass();
            String first = userReg.getFirst();
            String last = userReg.getLast();
            User attempt = userDao.getUserByEmail(email);
            if(attempt == null) {
                // If we didn't find a user then the email is available to use as a new account.
                User newUser = new User(0, first, last, email, pass);
                userDao.addUser(newUser);
                return Response.status(Response.Status.CREATED).build();
            }
            else {
                String response = new ResponseError("Email already used").toJson();
                return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
            }
        }
        catch(IOException ie) {
            String jsonError = new ResponseError("Badly formatted input.").toJson();
            return Response.status(Response.Status.BAD_REQUEST).entity(jsonError).build();
        }
    }

    private class UserRegistration {
        private String first;
        private String last;
        private String email;
        private String pass;

        public UserRegistration(String first, String last, String email, String pass) {
            this.first = first;
            this.last = last;
            this.email = email;
            this.pass = pass;
        }

        public String getFirst() {
            return first;
        }

        public String getLast() {
            return last;
        }

        public String getEmail() {
            return email;
        }

        public String getPass() {
            return pass;
        }
    }

    private class UserRegDeserialiser extends StdDeserializer<UserRegistration> {

        protected UserRegDeserialiser() {
            this(null);
        }

        protected UserRegDeserialiser(Class<?> vc) {
            super(vc);
        }

        @Override
        public UserRegistration deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            JsonNode node = jsonParser.getCodec().readTree(jsonParser);
            String first = node.get("first").asText();
            String last = node.get("last").asText();
            String email = node.get("email").asText();
            String password = node.get("password").asText();
            return new UserRegistration(first, last, email, password);
        }
    }
}
