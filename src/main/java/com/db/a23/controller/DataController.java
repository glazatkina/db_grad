package com.db.a23.controller;

import com.db.a23.auth.Authenticated;
import com.db.a23.beans.Counterparty;
import com.db.a23.beans.Deal;
import com.db.a23.dao.CounterpartyDAO;
import com.db.a23.dao.DealDAO;
import com.db.a23.dao.InstrumentDAO;
import com.db.a23.dao.impl.CounterpartyDAOImpl;
import com.db.a23.dao.impl.DealDAOImpl;
import com.db.a23.dao.impl.InstrumentDAOImpl;
import com.db.a23.json.CounterpartySerialiser;
import com.db.a23.json.DealSerialiser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;

@Path("data")
public class DataController {

    private DealDAO ddao;
    private InstrumentDAO idao;
    private CounterpartyDAO cdao;
    private ObjectMapper mapper;

    public DataController() {
        ddao = new DealDAOImpl();
        idao = new InstrumentDAOImpl();
        cdao = new CounterpartyDAOImpl();
        mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(Deal.class, new DealSerialiser());
        module.addSerializer(Counterparty.class, new CounterpartySerialiser());
        mapper.registerModule(module);
    }

    @GET
//    @Authenticated
    @Path("instruments")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getInstruments() {
        return Response.status(Response.Status.OK).build();
    }

    @GET
//    @Authenticated
    @Path("counterparty")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCounterParties() {
        Collection<Counterparty> cps = cdao.getAllCounterparties();
        try {
            String counterpartyJson = mapper.writeValueAsString(cps);
            return Response.status(Response.Status.OK).entity(counterpartyJson).build();
        }
        catch(JsonProcessingException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
//    @Authenticated
    @Path("counterparty/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCounterPartyById(@PathParam("id") long id) {
        Counterparty cps = cdao.getCounterpartyById(id);
        try {
            String counterpartyJson = mapper.writeValueAsString(cps);
            return Response.status(Response.Status.OK).entity(counterpartyJson).build();
        }
        catch(JsonProcessingException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
//    @Authenticated
    @Path("deals")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDeals() {
        Collection<Deal> deals = ddao.getAllDeals();
        try {
            String dealJson = mapper.writeValueAsString(deals);
            return Response.status(Response.Status.OK).entity(dealJson).build();
        } catch (JsonProcessingException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
//    @Authenticated
    @Path("deals/{counterpartyId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDealsByCounterparty(@PathParam("counterpartyId") long counterPartyId) {
        Collection<Deal> dealsByCounterpartyId = ddao.getAllDealsByCounterpartyId(counterPartyId);
        try {
            String dealJson = mapper.writeValueAsString(dealsByCounterpartyId);
            return Response.status(Response.Status.OK).entity(dealJson).build();
        } catch (JsonProcessingException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

}
