package com.db.a23.auth;

import javax.annotation.Priority;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.db.a23.beans.User;

/**
 * Token based authentication.
 * <p>
 * When a user makes a request to endpoints which are annotated with the {@link Authenticated}
 * label, the request header is checked for a token.
 * </p>
 * <p>
 * If the token exists in DB session table, the user is allowed to access the method. Ohterwise the
 * request fails with an un-authorized error.
 * </p>
 */
@Authenticated
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {
    public static final String AUTHENTICATION_SCHEME = "Bearer";

    @Override
    public void filter(final ContainerRequestContext requestContext) {
        final String authHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

        if (isTokenAuth(authHeader)) {
            final String token = extractToken(authHeader);
            try {
                final User user = validateToken(token);
                requestContext.setProperty("user", user);
            } catch (final Exception e) {
                abort(requestContext);
            }
        } else {
            abort(requestContext);
        }
    }

    /**
     * Parse the token string from the header.
     *
     * @param authHeader - Authorization header text.
     * @return the part after Authorization: Bearer.
     */
    public static String extractToken(final String authHeader) {
        return authHeader.substring(AUTHENTICATION_SCHEME.length()).trim();
    }

    /**
     * Is the Authorization type 'Bearer'?.
     *
     * @param authHeader - auth header text.
     * @return true if header auth option is Bearer.
     */
    private boolean isTokenAuth(final String authHeader) {
        return authHeader != null
                && authHeader.toLowerCase().startsWith(AUTHENTICATION_SCHEME.toLowerCase() + " ");
    }

    /**
     * Fail with unauthorized error code.
     *
     * @param requestContext incoming request.
     */
    private void abort(final ContainerRequestContext requestContext) {
        requestContext.abortWith(
                Response.status(Response.Status.UNAUTHORIZED)
                        .header(HttpHeaders.WWW_AUTHENTICATE, AUTHENTICATION_SCHEME)
                        .build());
    }

    private User validateToken(final String token) throws Exception {
//        final Session sess = Persistence.getInstance().loadSession(token);
//
//        if (sess != null && sess.isValid()) {
//            User user = Persistence.getInstance().loadUserById(sess.user);
//            if (user == null) {
//                // This causes abort with unauthorized
//                throw new NotFoundException("User not found");
//            }
//            return user;
//        } else {
//            // This also causes the abort
//            throw new NotAuthorizedException("Client not authorized");
//        }
        return new User(1, "Test", "User", "test@mail.com", "password");
    }
}