package com.db.a23.dao;

import com.db.a23.beans.Counterparty;

import java.util.Collection;

public interface CounterpartyDAO {
    Counterparty getCounterpartyById(long id);
    Collection<Counterparty> getAllCounterparties();
}
