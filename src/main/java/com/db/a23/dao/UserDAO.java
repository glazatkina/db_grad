package com.db.a23.dao;

import com.db.a23.beans.User;

import java.util.Collection;

public interface UserDAO {
    User getUserById(long id);
    User getUserByEmail(String email);
    Collection<User> getAllUsers();
    long addUser(User user);
}
