package com.db.a23.dao;

import com.db.a23.beans.DealType;

public interface DealTypeDAO {
    DealType getDealTypeById(long id);
}
