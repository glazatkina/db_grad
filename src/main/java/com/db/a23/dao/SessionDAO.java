package com.db.a23.dao;

import com.db.a23.beans.Session;

public interface SessionDAO {
    Session getSessionById(long id);
    Session getSessionByToken(String token);
    long addSession(Session session);
    boolean removeSessionByToken(String token);
}
