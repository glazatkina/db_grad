package com.db.a23.dao;

import com.db.a23.beans.Instrument;

import java.util.Collection;

public interface InstrumentDAO {
    Instrument getInstrumentById(long id);
    Collection<Instrument> getAllInstruments();
}
