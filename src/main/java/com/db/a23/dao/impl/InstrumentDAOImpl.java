package com.db.a23.dao.impl;

import com.db.a23.beans.Instrument;
import com.db.a23.dao.InstrumentDAO;
import com.db.a23.repository.MySqlConnector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.HashSet;

public class InstrumentDAOImpl implements InstrumentDAO {
    private Connection connection;

    @Override
    public Instrument getInstrumentById(long id) {
        Instrument instrument = new Instrument();
        try {
            connection = MySqlConnector.getMySqlConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT instrument_id, instrument_name " +
                            "FROM db_grad_cs_1917.instrument WHERE instrument_id = ? ;");

            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                instrument.setId(resultSet.getLong("instrument_id"));
                instrument.setInstrumentName(resultSet.getString("instrument_name"));
            }
        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        } finally {
            return instrument;
        }
    }

    @Override
    public Collection<Instrument> getAllInstruments() {
        Collection<Instrument> instruments = new HashSet<>();
        try {
            connection = MySqlConnector.getMySqlConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT instrument_id, instrument_name " +
                            "FROM db_grad_cs_1917.instrument");

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Instrument instrument = new Instrument();
                instrument.setId(resultSet.getLong("instrument_id"));
                instrument.setInstrumentName(resultSet.getString("instrument_name"));
                instruments.add(instrument);
            }
        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        } finally {
            return instruments;
        }
    }
}
