package com.db.a23.dao.impl;

import com.db.a23.beans.CounterpartyStatus;
import com.db.a23.dao.CounterpartyStatusDAO;
import com.db.a23.repository.MySqlConnector;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class CounterpartyStatusDAOImpl implements CounterpartyStatusDAO {
    private Connection connection;

    @Override
    public CounterpartyStatus getCounterpartyStatusById(long id) {
        CounterpartyStatus counterpartyStatus = new CounterpartyStatus();
        try {
            connection = MySqlConnector.getMySqlConnection();
            String sql = "SELECT counterparty_status_id, status FROM mydb.counterparty_status WHERE counterparty_status_id = " + id + ";";
            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                counterpartyStatus.setId(resultSet.getLong("counterparty_status_id"));
                counterpartyStatus.setStatus(resultSet.getString("status"));
            }
        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        } finally {
            return counterpartyStatus;
        }
    }
}
