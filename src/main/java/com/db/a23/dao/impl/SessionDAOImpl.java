package com.db.a23.dao.impl;

import com.db.a23.beans.Session;
import com.db.a23.dao.SessionDAO;
import com.db.a23.repository.MySqlConnector;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class SessionDAOImpl implements SessionDAO {
    private Connection connection;

    @Override
    public Session getSessionById(long id) {
        Session session = new Session();
        try {
            connection = MySqlConnector.getMySqlConnection();
            String sql = "SELECT session_id, user_id, token, expire_date " +
                    "FROM mydb.session WHERE session_id = " + id + ";";
            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                session = fetchSession(resultSet);
            }
        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        } finally {
            return session;
        }
    }

    @Override
    public Session getSessionByToken(String token) {
        Session session = new Session();
        try {
            connection = MySqlConnector.getMySqlConnection();
            String sql = "SELECT session_id, user_id, token, expire_date " +
                    "FROM mydb.session WHERE token = '" + token + "';";
            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                session = fetchSession(resultSet);
            }
        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        } finally {
            return session;
        }
    }

    /**
     *
     * @param session
     * @return id of added session; -1 if none was added
     */
    @Override
    public long addSession(Session session) {
        try {
            connection = MySqlConnector.getMySqlConnection();

            String sql = "INSERT INTO mydb.session " +
                    "(user_id, token, expire_date)" +
                    "VALUES ('"+ session.getUserId() + "', '" + session.getToken() + "', '" +
                    session.getExpireDate().format(DateTimeFormatter.ISO_DATE) + "');";

            Statement statement = connection.createStatement();
            int i = statement.executeUpdate(sql);

            if (i == 1) {
                return getSessionByToken(session.getToken()).getId();
            }
            return -1;
        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
            return -1;
        }
    }

    @Override
    public boolean removeSessionByToken(String token) {
        try {
            connection = MySqlConnector.getMySqlConnection();

            long sessionId = getSessionByToken(token).getId();

            String sql = "DELETE FROM mydb.session " +
                    "WHERE session_id = " + sessionId + ";";
            
            Statement statement = connection.createStatement();
            int i = statement.executeUpdate(sql);
            return i == 1;
        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
            return false;
        }

    }

    private Session fetchSession(ResultSet resultSet) throws SQLException {
        Session session = new Session();
        session.setId(resultSet.getLong("session_id"));
        session.setUserId(resultSet.getLong("user_id"));
        session.setToken(resultSet.getString("token"));
        session.setExpireDate(ZonedDateTime.ofInstant(Instant.ofEpochMilli(resultSet.getTimestamp("expire_date").getTime()), ZoneOffset.UTC));
        return session;
    }
}
