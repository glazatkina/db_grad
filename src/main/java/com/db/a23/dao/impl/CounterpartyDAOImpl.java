package com.db.a23.dao.impl;

import com.db.a23.beans.Counterparty;
import com.db.a23.beans.CounterpartyStatus;
import com.db.a23.dao.CounterpartyDAO;
import com.db.a23.repository.MySqlConnector;

import java.sql.*;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.HashSet;

public class CounterpartyDAOImpl implements CounterpartyDAO {
    private Connection connection;

    @Override
    public Counterparty getCounterpartyById(long id) {
        Counterparty counterparty = new Counterparty();
        try {
            connection = MySqlConnector.getMySqlConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT counterparty_id, counterparty_name, " +
                            "counterparty_status, counterparty_date_registered " +
                            "FROM db_grad_cs_1917.counterparty WHERE counterparty_id = ? ;");
            statement.setLong(1, id);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                counterparty = fetchCounterparty(resultSet);
            }
        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        } finally {
            return counterparty;
        }
    }

    @Override
    public Collection<Counterparty> getAllCounterparties() {
        Collection<Counterparty> counterparties = new HashSet<>();
        try {
            connection = MySqlConnector.getMySqlConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT counterparty_id, counterparty_name, " +
                            "counterparty_status, counterparty_date_registered " +
                            "FROM db_grad_cs_1917.counterparty;");

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                counterparties.add(fetchCounterparty(resultSet));
            }
        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        } finally {
            return counterparties;
        }
    }

    private Counterparty fetchCounterparty(ResultSet resultSet) throws SQLException {
        Counterparty counterparty = new Counterparty();
        counterparty.setId(resultSet.getLong("counterparty_id"));
        counterparty.setCounterpartyName(resultSet.getString("counterparty_name"));
        counterparty.setCounterpartyStatus(new CounterpartyStatus(1, "A"));
        counterparty.setCounterpartyDateRegistered(ZonedDateTime.ofInstant(Instant.ofEpochMilli(resultSet.getTimestamp("counterparty_date_registered").getTime()), ZoneOffset.UTC));
        return counterparty;
    }
}
