package com.db.a23.dao.impl;

import com.db.a23.beans.DealType;
import com.db.a23.dao.DealTypeDAO;
import com.db.a23.repository.MySqlConnector;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class DealTypeDAOImpl implements DealTypeDAO {
    private Connection connection;

    @Override
    public DealType getDealTypeById(long id) {
        DealType dealType = new DealType();
        try {
            connection = MySqlConnector.getMySqlConnection();
            String sql = "SELECT deal_type_id, deal_type FROM mydb.deal_type WHERE deal_type_id = " + id + ";";
            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                dealType.setId(resultSet.getLong("deal_type_id"));
                dealType.setDealType(resultSet.getString("deal_type"));
            }
        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        } finally {
            return dealType;
        }
    }
}
