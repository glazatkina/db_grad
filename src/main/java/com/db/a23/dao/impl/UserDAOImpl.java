package com.db.a23.dao.impl;

import com.db.a23.beans.User;
import com.db.a23.dao.UserDAO;
import com.db.a23.repository.MySqlConnector;

import java.sql.*;
import java.util.Collection;
import java.util.HashSet;

public class UserDAOImpl implements UserDAO {
    private Connection connection;

    @Override
    public User getUserById(long id) {
        User user = null;
        try {
            connection = MySqlConnector.getMySqlConnection();
            String sql = "SELECT user_id, first_name, last_name, email, password, salt FROM mydb.users WHERE user_id = " + id + ";";
            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                user = fetchUser(resultSet);
            }
        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        } finally {
            return user;
        }
    }

    @Override
    public User getUserByEmail(String email) {
        User user = null;
        try {
            connection = MySqlConnector.getMySqlConnection();

            PreparedStatement statement = connection.prepareStatement(
                    "SELECT user_id, user_pwd " +
                            "FROM db_grad_cs_1917.users WHERE user_id = ?;");
            statement.setString(1, email);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                user = fetchUser(resultSet);
            }
        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        } finally {
            return user;
        }
    }

    @Override
    public Collection<User> getAllUsers() {
        Collection<User> users = new HashSet<>();
        try {
            connection = MySqlConnector.getMySqlConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT user_id, user_pwd " +
                            "FROM db_grad_cs_1917.users;");

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                users.add(fetchUser(resultSet));
            }
        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        } finally {
            return users;
        }
    }

    /**
     *
     * @param user user for inserting into table
     * @return user_id that was created, or -1 if creation was failed
     */
    @Override
    public long addUser(User user) {
        try {
            connection = MySqlConnector.getMySqlConnection();

//            String sql = "INSERT  INTO mydb.users " +
//                    "(first_name, last_name, " +
//                    "email, password, salt)" +
//                    "VALUES ('"+ user.getFirstName() + "', '" + user.getLastName() + "', '" +
//                    user.getEmail() + "', '" + user.getPassword() + "', '" + user.getSalt() + "');";

            Statement statement = connection.createStatement();
            int i = statement.executeUpdate("");

            if (i == 1) {
                return getUserByEmail(user.getEmail()).getUserId();
            }
            return -1;
        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
            return -1;
        }
    }

    private static User fetchUser(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setUserId(1);
        user.setFirstName("Name");
        user.setLastName("Last Name");
        user.setEmail(resultSet.getString("user_id"));
        user.setPassword(resultSet.getString("user_pwd"));
//        user.setSalt(resultSet.getString("salt"));
        return user;
    }
}
