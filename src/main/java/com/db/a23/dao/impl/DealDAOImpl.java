package com.db.a23.dao.impl;

import com.db.a23.beans.*;
import com.db.a23.dao.DealDAO;
import com.db.a23.repository.MySqlConnector;

import java.math.BigDecimal;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;

public class DealDAOImpl implements DealDAO {
    Connection connection;

    @Override
    public Deal getDealById(long id) {
        Deal deal = new Deal();
        try {
            connection = MySqlConnector.getMySqlConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT d.deal_id, d.deal_type, d.deal_amount, d.deal_time, d.deal_quantity, " +
                            "c.counterparty_id, c.counterparty_name, c.counterparty_status, " +
                            "c.counterparty_date_registered, " +
                            "i.instrument_id, i.instrument_name " +
                            "FROM db_grad_cs_1917.deal d " +
                            "JOIN db_grad_cs_1917.counterparty c " +
                            "ON (c.counterparty_id = d.deal_counterparty_id) " +
                            "JOIN db_grad_cs_1917.instrument i " +
                            "ON (i.instrument_id = d.deal_instrument_id) " +
                            "WHERE deal_id = ? ;");
            statement.setLong(1, id);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                deal = fetchDeal(resultSet);
            }
        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        } finally {
            return deal;
        }
    }

    @Override
    public Collection<Deal> getAllDeals() {
        Collection<Deal> deals = new HashSet<>();
        try {
            connection = MySqlConnector.getMySqlConnection();

            PreparedStatement statement = connection.prepareStatement(
                    "SELECT d.deal_id, d.deal_type, d.deal_amount, d.deal_time, d.deal_quantity, " +
                            "c.counterparty_id, c.counterparty_name, c.counterparty_status, " +
                            "c.counterparty_date_registered, " +
                            "i.instrument_id, i.instrument_name " +
                            "FROM db_grad_cs_1917.deal d " +
                            "JOIN db_grad_cs_1917.counterparty c " +
                            "ON (c.counterparty_id = d.deal_counterparty_id) " +
                            "JOIN db_grad_cs_1917.instrument i " +
                            "ON (i.instrument_id = d.deal_instrument_id);");

            ResultSet resultSet = statement.executeQuery();

            deals = fetchDealsCollection(resultSet);

        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        } finally {
            return deals;
        }
    }

    @Override
    public Collection<Deal> getAllDealsByCounterpartyId(long counterpartyId) {
        Collection<Deal> deals = new HashSet<>();
        try {
            connection = MySqlConnector.getMySqlConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT d.deal_id, d.deal_type, d.deal_amount, d.deal_time, d.deal_quantity,  " +
                            "c.counterparty_id, c.counterparty_name, c.counterparty_status, " +
                            "c.counterparty_date_registered, " +
                            "i.instrument_id, i.instrument_name " +
                            "FROM db_grad_cs_1917.deal d " +
                            "JOIN db_grad_cs_1917.counterparty c " +
                            "ON (c.counterparty_id = d.deal_counterparty_id) " +
                            "JOIN db_grad_cs_1917.instrument i " +
                            "ON (i.instrument_id = d.deal_instrument_id) " +
                            "WHERE d.deal_counterparty_id = ? ;");
            statement.setLong(1, counterpartyId);

            ResultSet resultSet = statement.executeQuery();

            deals = fetchDealsCollection(resultSet);

        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        } finally {
            return deals;
        }
    }

    @Override
    public Collection<Deal> getAllDealsByInstrumentId(long instrumentId) {
        Collection<Deal> deals = new HashSet<>();
        try {
            connection = MySqlConnector.getMySqlConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT d.deal_id, d.deal_type, d.deal_amount, d.deal_time, d.deal_quantity,  " +
                            "c.counterparty_id, c.counterparty_name, c.counterparty_status, " +
                            "c.counterparty_date_registered, " +
                            "i.instrument_id, i.instrument_name " +
                            "FROM db_grad_cs_1917.deal d " +
                            "JOIN db_grad_cs_1917.counterparty c " +
                            "ON (c.counterparty_id = d.deal_counterparty_id) " +
                            "JOIN db_grad_cs_1917.instrument i " +
                            "ON (i.instrument_id = d.deal_instrument_id) " +
                            "WHERE d.deal_instrument_id = ? ;");
            statement.setLong(1, instrumentId);

            ResultSet resultSet = statement.executeQuery();

            deals = fetchDealsCollection(resultSet);

        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        } finally {
            return deals;
        }
    }

    @Override
    public Map<Instrument, Long> getAllInstrumentsQuantity() {
        Map<Instrument, Long> instrumentIntegerMap = new HashMap<>();
        try {
            connection = MySqlConnector.getMySqlConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT count(1) as deals_quantity, instrument_name, d.deal_instrument_id " +
                            "FROM db_grad_cs_19171.deal d " +
                            "JOIN db_grad_cs_1917.instrument i " +
                            "ON (i.instrument_id = d.deal_instrument_id) " +
                            "group by d.deal_instrument_id;");

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Instrument instrument = new Instrument();
                instrument.setId(resultSet.getLong("deal_instrument_id"));
                instrument.setInstrumentName(resultSet.getString("instrument_name"));
                long quantity = resultSet.getLong("deals_quantity");

                instrumentIntegerMap.put(instrument, quantity);
            }

        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        } finally {
            return instrumentIntegerMap;
        }
    }

    @Override
    public Map<Counterparty, BigDecimal> getAllCounterpartiesAmount() {
        Map<Counterparty, BigDecimal> counterpartyAmount = new HashMap<>();
        try {
            connection = MySqlConnector.getMySqlConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT sum(deal_amount) as amount, c.counterparty_name, " +
                            "c.counterparty_id, c.counterparty_status, " +
                            "c.counterparty_date_registered " +
                            "FROM db_grad_cs_1917.deal d " +
                            "JOIN db_grad_cs_1917.counterparty c " +
                            "ON (c.counterparty_id = d.deal_counterparty_id) " +
                            "GROUP BY d.deal_counterparty_id;");

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Counterparty counterparty = new Counterparty();
                counterparty.setId(resultSet.getLong("counterparty_id"));
                counterparty.setCounterpartyName(resultSet.getString("counterparty_name"));
                counterparty.setCounterpartyStatus(new CounterpartyStatus(1, "A"));
                counterparty.setCounterpartyDateRegistered(ZonedDateTime.ofInstant(Instant.ofEpochMilli(resultSet.getTimestamp("counterparty_date_registered").getTime()), ZoneOffset.UTC));

                BigDecimal amount = resultSet.getBigDecimal("amount");

                counterpartyAmount.put(counterparty, amount);
            }

        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        } finally {
            return counterpartyAmount;
        }
    }

    private Deal fetchDeal(ResultSet resultSet) throws SQLException {
        Deal deal = new Deal();
        Counterparty counterparty = new Counterparty();
        Instrument instrument = new Instrument();
        DealType dealType = new DealType();

        dealType.setId(1);
        dealType.setDealType(resultSet.getString("deal_type"));
        deal.setDealType(dealType);

        instrument.setId(resultSet.getLong("instrument_id"));
        instrument.setInstrumentName(resultSet.getString("instrument_name"));
        deal.setInstrument(instrument);

        counterparty.setId(resultSet.getLong("counterparty_id"));
        counterparty.setCounterpartyName(resultSet.getString("counterparty_name"));
        counterparty.setCounterpartyStatus(new CounterpartyStatus(1, "A"));
        counterparty.setCounterpartyDateRegistered(ZonedDateTime.ofInstant(Instant.ofEpochMilli(resultSet.getTimestamp("counterparty_date_registered").getTime()), ZoneOffset.UTC));
        deal.setCounterparty(counterparty);

        deal.setId(resultSet.getLong("deal_id"));
        deal.setDealAmount(resultSet.getBigDecimal("deal_amount"));
        deal.setDealQuantity(resultSet.getLong("deal_quantity"));


        String string = resultSet.getString("deal_time");
        String ISO_8601_24H_FULL_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
        SimpleDateFormat sdf = new SimpleDateFormat(ISO_8601_24H_FULL_FORMAT);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        ZonedDateTime dateTime = null;
        try {
            dateTime = ZonedDateTime.ofInstant(sdf.parse(string).toInstant(), ZoneOffset.UTC);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        deal.setDealTime(dateTime);

        return deal;
    }

    private Collection<Deal> fetchDealsCollection(ResultSet resultSet) throws SQLException {
        Collection<Deal> deals = new HashSet<>();
        while (resultSet.next()) {
            deals.add(fetchDeal(resultSet));
        }
        return deals;
    }
}
