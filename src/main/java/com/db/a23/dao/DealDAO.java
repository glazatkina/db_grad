package com.db.a23.dao;

import com.db.a23.beans.Counterparty;
import com.db.a23.beans.Deal;
import com.db.a23.beans.Instrument;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

public interface DealDAO {
    Deal getDealById(long id);
    Collection<Deal> getAllDeals();
    Collection<Deal> getAllDealsByCounterpartyId(long counterpartyId);
    Collection<Deal> getAllDealsByInstrumentId(long instrumentId);
    Map<Instrument, Long> getAllInstrumentsQuantity();
    Map<Counterparty, BigDecimal> getAllCounterpartiesAmount();
}
