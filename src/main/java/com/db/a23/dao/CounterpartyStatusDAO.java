package com.db.a23.dao;

import com.db.a23.beans.CounterpartyStatus;

public interface CounterpartyStatusDAO {
    CounterpartyStatus getCounterpartyStatusById(long id);
}
