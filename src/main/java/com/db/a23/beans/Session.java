package com.db.a23.beans;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class Session {

    private static final ZoneId ZONE = ZoneId.of("GMT");
    private long id;
    private long userId;
    private String token;
    private ZonedDateTime expireDate;
    public static final long WEEK_TTL = 60 * 60 * 24 * 7;  // Week in seconds. Standard session expiry date.

    public Session() {}

    public Session(long id, User user, String token) {
        this.id = id;
        this.userId = user.getUserId();
        this.token = token;
        this.expireDate = ZonedDateTime.now().plusSeconds(WEEK_TTL);
    }

    public Session(long id, User user, String token, long timeToLive) {
        this.id = id;
        this.userId = user.getUserId();
        this.token = token;
        final ZonedDateTime now = ZonedDateTime.now(ZONE);
        this.expireDate = now.plusSeconds(timeToLive);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(User user) {
        this.userId = user.getUserId();
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ZonedDateTime getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(ZonedDateTime expireDate) {
        this.expireDate = expireDate;
    }

    @Override
    public String toString() {
        return "Session{" +
                "id=" + id +
                ", userId=" + userId +
                ", token='" + token + '\'' +
                ", expireDate=" + expireDate +
                '}';
    }
}
