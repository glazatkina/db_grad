package com.db.a23.beans;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class User {

    private long userId;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
//    private String salt;

    public User() {
    }

    public User(int userId,
                //constructor for new users
                String firstName,
                String lastName,
                String email,
                String password) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
//        this.salt = salt;
    }

//    public User(int userId,
//        //constructor for new users
//                String firstName,
//                String lastName,
//                String email,
//                String password) {
//        this.userId = userId;
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.email = email;
////        this.salt = generateSalt();
////        this.password = hashPassword(password);
//        this.password =
//
//    }

    public User(String salt, String password){
        //constructor for returning users

    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Blame Selvyn for removal of password hashing and salting
     * - BECAUSE WHO CARES ABOUT SECURITY.
     */
//    public String getSalt() {
//        return salt;
//    }
//
//    public void setSalt(String salt) {
//        this.salt = salt;
//    }
//
//    @Override
//    public String toString() {
//        return "User{" +
//                "userId=" + userId +
//                ", firstName='" + firstName + '\'' +
//                ", lastName='" + lastName + '\'' +
//                ", email='" + email + '\'' +
//                ", password='" + password + '\'' +
//                ", salt='" + salt + '\'' +
//                '}';
//    }
//
//    private static String bytesToHex(byte[] hash) {
//        StringBuffer hexString = new StringBuffer();
//        for (int i = 0; i < hash.length; i++) {
//            String hex = Integer.toHexString(0xff & hash[i]);
//            if(hex.length() == 1) hexString.append('0');
//            hexString.append(hex);
//        }
//        return hexString.toString();
//
//    }
//
//    public String hashPassword(String password){
//        //append password with unique salt, and then hash (SHA-256)
//        MessageDigest digest;
//        String defaultPassword = "default";
//        String salt = this.getSalt();
//        try {
//            digest = MessageDigest.getInstance("SHA-256");
//            String saltedPassword = saltPassword(salt, password);
//            byte[] byteString = saltedPassword.getBytes();
//            byte[] encodedHash = digest.digest(byteString);
//            return bytesToHex(encodedHash);
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//        return defaultPassword;
//
//    }
//
//    public String generateSalt(){
//        SecureRandom random = new SecureRandom();
//        byte bytes[] = new byte[20];
//        random.nextBytes(bytes);
//        String hexSalt = bytesToHex(bytes);
//        return hexSalt;
//
//    }
//
//    public String saltPassword(String salt, String password){
//        String saltPassword = password + salt;
//        return saltPassword;
//
//    }


    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
