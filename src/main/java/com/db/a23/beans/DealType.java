package com.db.a23.beans;

import javax.persistence.*;
import java.util.Objects;

public class DealType {
    private long id;
    private String dealType;

    public DealType() {
    }

    public DealType(int id, String dealType) {
        this.id = id;
        this.dealType = dealType;
    }

    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    
    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DealType dealType1 = (DealType) o;
        return dealType.equals(dealType1.dealType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dealType);
    }

    @Override
    public String toString() {
        return "DealType{" +
                "id=" + id +
                ", dealType='" + dealType + '\'' +
                '}';
    }

    public boolean isSell() {
        return dealType.equals("S");
    }
}
