package com.db.a23.beans;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

public class CounterpartyStatus {
    private long id;
    private String status;

    public CounterpartyStatus() {
    }

    public CounterpartyStatus(long id, String status) {
        this.id = id;
        this.status = status;
    }

    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "CounterpartyStatus{" +
                "id=" + id +
                ", status='" + status + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CounterpartyStatus that = (CounterpartyStatus) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, status);
    }
}
