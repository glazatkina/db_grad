package com.db.a23.beans;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.ZonedDateTime;


public class Counterparty {
    private long id;
    private String counterpartyName;
    private ZonedDateTime counterpartyDateRegistered;
    private CounterpartyStatus counterpartyStatus;

    public Counterparty() {
    }

    public Counterparty(long id, String counterpartyName, ZonedDateTime counterpartyDateRegistered, CounterpartyStatus counterpartyStatus) {
        this.id = id;
        this.counterpartyName = counterpartyName;
        this.counterpartyDateRegistered = counterpartyDateRegistered;
        this.counterpartyStatus = counterpartyStatus;
    }

    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    
    public String getCounterpartyName() {
        return counterpartyName;
    }

    public void setCounterpartyName(String counterpartyName) {
        this.counterpartyName = counterpartyName;
    }

    
    public ZonedDateTime getCounterpartyDateRegistered() {
        return counterpartyDateRegistered;
    }

    public void setCounterpartyDateRegistered(ZonedDateTime counterpartyDateRegistered) {
        this.counterpartyDateRegistered = counterpartyDateRegistered;
    }

    
    public CounterpartyStatus getCounterpartyStatus() {
        return counterpartyStatus;
    }

    public void setCounterpartyStatus(CounterpartyStatus counterpartyStatus) {
        this.counterpartyStatus = counterpartyStatus;
    }

    @Override
    public String toString() {
        return "Counterparty{" +
                "id=" + id +
                ", counterpartyName='" + counterpartyName + '\'' +
                ", counterpartyDateRegistered=" + counterpartyDateRegistered +
                ", counterpartyStatus=" + counterpartyStatus +
                '}';
    }
}
