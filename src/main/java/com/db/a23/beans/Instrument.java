package com.db.a23.beans;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

public class Instrument {
    private long id;
    private String instrumentName;

    public Instrument() {
    }

    public Instrument(int id, String instrumentName) {
        this.id = id;
        this.instrumentName = instrumentName;
    }

    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    
    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Instrument that = (Instrument) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, instrumentName);
    }

    @Override
    public String toString() {
        return "Instrument{" +
                "id=" + id +
                ", instrumentName='" + instrumentName + '\'' +
                '}';
    }
}
