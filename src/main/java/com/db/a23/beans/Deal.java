package com.db.a23.beans;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Date;

public class Deal {
    private long id;
    private ZonedDateTime dealTime;
    private BigDecimal dealAmount;
    private long dealQuantity;
    private DealType dealType;
    private Instrument instrument;
    private Counterparty counterparty;

    public Deal() {
    }

    public Deal(int id, ZonedDateTime dealTime, BigDecimal dealAmount, long dealQuantity, DealType dealType, Instrument instrument, Counterparty counterparty) {
        this.id = id;
        this.dealTime = dealTime;
        this.dealAmount = dealAmount;
        this.dealQuantity = dealQuantity;
        this.dealType = dealType;
        this.instrument = instrument;
        this.counterparty = counterparty;
    }

    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    
    public ZonedDateTime getDealTime() {
        return dealTime;
    }

    public void setDealTime(ZonedDateTime dealTime) {
        this.dealTime = dealTime;
    }

    
    public BigDecimal getDealAmount() {
        return dealAmount;
    }

    public void setDealAmount(BigDecimal dealAmount) {
        this.dealAmount = dealAmount;
    }

    
    public long getDealQuantity() {
        return dealQuantity;
    }

    public void setDealQuantity(long dealQuantity) {
        this.dealQuantity = dealQuantity;
    }

    
    public DealType getDealType() {
        return dealType;
    }

    public void setDealType(DealType dealType) {
        this.dealType = dealType;
    }

    
    public Instrument getInstrument() {
        return instrument;
    }

    public void setInstrument(Instrument instrument) {
        this.instrument = instrument;
    }

    
    public Counterparty getCounterparty() {
        return counterparty;
    }

    public void setCounterparty(Counterparty counterparty) {
        this.counterparty = counterparty;
    }

    @Override
    public String toString() {
        return "Deal{" +
                "id=" + id +
                ", dealTime=" + dealTime +
                ", dealAmount=" + dealAmount +
                ", dealQuantity=" + dealQuantity +
                ", dealType=" + dealType +
                ", instrument=" + instrument +
                ", counterparty=" + counterparty +
                '}';
    }

    public boolean isSellDeal() {
        return dealType.isSell();
    }
}
