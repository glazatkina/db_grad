package com.db.a23.json;

import com.db.a23.beans.Deal;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class DealSerialiser extends StdSerializer<Deal> {

    public DealSerialiser() {
        this(null);
    }

    public DealSerialiser(Class<Deal> t) {
        super(t);
    }

    @Override
    public void serialize(Deal deal, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("deal_id", deal.getId());
        jsonGenerator.writeNumberField("deal_amount", deal.getDealAmount());
        jsonGenerator.writeNumberField("deal_quantity", deal.getDealQuantity());
        jsonGenerator.writeNumberField("deal_time", deal.getDealTime().toEpochSecond());
        jsonGenerator.writeStringField("deal_type", deal.getDealType().getDealType());
        jsonGenerator.writeStringField("instrument", deal.getInstrument().getInstrumentName());
        jsonGenerator.writeObjectFieldStart("counterparty");
        jsonGenerator.writeNumberField("id", deal.getCounterparty().getId());
        jsonGenerator.writeStringField("counterparty_name", deal.getCounterparty().getCounterpartyName());
        jsonGenerator.writeNumberField("counterparty_date_registered", deal.getCounterparty().getCounterpartyDateRegistered().toEpochSecond());
        jsonGenerator.writeStringField("counterparty_status", deal.getCounterparty().getCounterpartyStatus().getStatus());
        jsonGenerator.writeEndObject();
        jsonGenerator.writeEndObject();
    }
}