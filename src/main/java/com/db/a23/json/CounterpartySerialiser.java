package com.db.a23.json;

import com.db.a23.beans.Counterparty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class CounterpartySerialiser extends StdSerializer<Counterparty> {

    public CounterpartySerialiser() {
        this(null);
    }

    public CounterpartySerialiser(Class<Counterparty> t) {
        super(t);
    }

    @Override
    public void serialize(Counterparty counterparty, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("id", counterparty.getId());
        jsonGenerator.writeStringField("counterparty_name", counterparty.getCounterpartyName());
        jsonGenerator.writeNumberField("counterparty_date_registered", counterparty.getCounterpartyDateRegistered().toEpochSecond());
        jsonGenerator.writeStringField("counterparty_status", counterparty.getCounterpartyStatus().getStatus());
        jsonGenerator.writeEndObject();
    }
}
