package com.db.a23.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySqlConnector {
    public static Connection getMySqlConnection() throws ClassNotFoundException, SQLException {
        final String hostName = "10.11.32.21:3306";
        final String dbName = "db_grad_cs_1917";
        final String userName = "dbgrad";
        final String password = "dbgrad";
        return getMySqlConnection(hostName, dbName, userName, password);
    }

    private static Connection getMySqlConnection(
            String hostName, String dbName,
            String userName, String password) throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");

        String connectionUrl = String.format("jdbc:mysql://%s/%s", hostName, dbName);

        return DriverManager.getConnection(connectionUrl, userName, password);
    }
}
