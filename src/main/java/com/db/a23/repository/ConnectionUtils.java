package com.db.a23.repository;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionUtils {

    public static Connection getConnection()
            throws SQLException, ClassNotFoundException {
        return MySqlConnector.getMySqlConnection();
    }

    public static void closeConnection(Connection connection) {
        try{
            connection.close();
        } catch (Exception ignored){

        }
    }

    public static void rollbackTransaction (Connection connection) {
        try{
            connection.rollback();
        } catch (Exception ignored){

        }
    }
}
